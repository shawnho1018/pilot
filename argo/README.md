# Demo Argo Rollout
This project is to demonstrate argo rollout's progressive capability. 
## Demo 1: ReplicaSet as canary deployment base
Please run demo-1.sh to see how Argo utilizes replicaset to execute the canary deployment. This demo has no analysis and require user to manually promote (or with argo UI) the rollout.

## Demo 2: Istio's VS as the canary base
Please run demo-2.sh to see how Argo utilizes weight ratio to run the canary deployment. This demo also uses prometheus to measure the success-rate of the canary deployment. 


