#!/bin/zsh

function change_color() {
  color=$1
  kubectl argo rollouts set image rollouts-demo \
    rollouts-demo=argoproj/rollouts-demo:${color}
}

echo "Demo rollout with replicasets"
echo "Please use kubectl argo rollouts dashboard to demo the UI control"
echo "kubectl argo rollouts get rollouts rollouts-demo --watch could view the progress"
kubectl apply -f istio-rollout/
while true; do
   vared -p "Choose a color or type none to stop the demo: " -c decision
   if [ "${decision}" = "none" ]; then
      break
   else
      change_color ${decision}
   fi
   sleep 2
done
kubectl delete -f istio-rollout/
