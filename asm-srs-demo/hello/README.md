# Locality High Availability
This project is to demonstrate how to use istio to create localized High availability. The demo is a simplied version from [locality failover example](https://istio.io/latest/docs/tasks/traffic-management/locality-load-balancing/failover/) in istio. The key for this demo is the ds-locality file, which could could be used to assign failover from pod with label r1 to that with label r2. 
```
    loadBalancer:
      simple: ROUND_ROBIN
      localityLbSetting:
        enabled: true
        failover:
          - from: r1
            to: r2
```

In order to utilize this, users need to prepare at least 2 GKE clusters, pre-configured to trust each other. For those who dont know how to begin, please refer to my [anthos-dual project](https://gitlab.com/shawnho1018/pilot/-/tree/master/sre/anthos-dual) which could create such a cluster-pair with terraform. This project, by default, uses ASM managed control plane and is using ASM version 1.9. For any question, please leave me an issue. 