#!/bin/zsh
context1="gke_shawn-demo-2021_asia-east1-a_cluster-17"
context2="gke_shawn-demo-2021_asia-northeast1-a_cluster-17bk"

for ctx in $context1 $context2; do

cat <<EOF > sample.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: sample
  labels:
    istio.io/rev: asm-managed
EOF

kubectl apply -f ./sample.yaml --context $ctx
done

for reg in "r1.z1" "r1.z2" "r2.z1" "r2.z2"; do
  ./gen-helloworld.sh --version "$reg" > "helloworld-$reg.yaml"
done 

kubectl apply -f helloworld-r1.z1.yaml -n sample --context $context1
kubectl apply -f helloworld-r2.z1.yaml -n sample --context $context2
kubectl apply -f locality.yaml -n sample --context $context1
kubectl apply -f ./hello-gateway.yaml -- context $context1
kubectl apply -f sleep.yaml -n sample --context $context1
