# Demo Cross-Cluster Traffic Control using ASM managed kubernetes clusters
This folder provides two ASM traffic control mechanisms to achieve cross-cluster requests routing. The objective of this project is to demonstrate the flexibility of ASM. The sample pod uses opensourced [srs service](https://github.com/ossrs/srs). Stream file are first copied into the target pod. Then, a play_stream function is provided in [utility.sh](./asm-localized-lb/utility.sh) to quickly load the stream onto srs server for end viewers. 

## asm-localized-lb/ 
This case study showed how to use destinationrule to create an High-availability scenario for cross cluster traffic. The detailed topology is shown in ![](images/localized-lb.png)

This demonstration is built upon two GKE clusters and have been configured for cross-cluster connection with ASM. If you could use terraform, this folder projects a way to quickly setup a pair of GKE clusters which has been configured to have mutual trust. Once both clusters are ready, please retrieve their contexts. Then, they could directly provide both contexts in install.sh. Then, run the [install.sh](./install.sh) script which could quickly configured service mesh components, such as destinationrules, gateway, and virtualservices. 

## asm-hostname/
This case study showed how to use kubernetes services object to create different type of services, including remote-only (traffic only goes cross-cluster), local-only (traffic stays in the local cluster), and high-availability. The detailed topolgy can be viewed from ![](images/hostname.png)
