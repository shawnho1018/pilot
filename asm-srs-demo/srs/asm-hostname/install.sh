#!/bin/zsh
source utility.sh
context1="gke_shawn-demo-2021_asia-east1-a_cluster-17"
context2="gke_shawn-demo-2021_asia-northeast1-a_cluster-17bk"

for ctx in $context1 $context2; do
    kubectx $ctx
    kubectl create ns srs
    kubectl label ns srs istio.io/rev=asm-managed
    if [[ "$ctx" == "gke_shawn-demo-2021_asia-east1-a_cluster-17" ]]; then
      kubectl apply -f deployment-tw.yaml
    elif [[ "$ctx" == "gke_shawn-demo-2021_asia-northeast1-a_cluster-17bk" ]] then
      kubectl apply -f deployment-jp.yaml 
    fi
    sleep 3
    copy_file $ctx
    kubectl apply -f service-tw.yaml
    kubectl apply -f service-jp.yaml
done
kubectl apply -f srs-gateway-jp.yaml --context $context1 
kubectl apply -f srs-gateway-tw.yaml --context $context1
