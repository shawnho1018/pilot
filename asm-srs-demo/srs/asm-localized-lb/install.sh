#!/bin/zsh
context1="gke_shawn-demo-2021_asia-east1-a_cluster-17"
context2="gke_shawn-demo-2021_asia-northeast1-a_cluster-17bk"

for reg in "r1.z1" "r2.z1"; do
  ./gen-srs.sh --version "$reg" > "deployment-$reg.yaml"
done 

function copy_file {
  file=$1
  pod=$(kubectl get pods -l app=srs -n srs | grep srs | awk '{print $1}')
  kubectl wait --for=condition=ready pod ${pod} -n srs
  kubectl cp ${file} srs/$pod:/usr/local/srs -c srs
}

for ctx in $context1 $context2; do
    kubectx $ctx
    kubectl create ns srs
    kubectl label ns srs istio.io/rev=asm-managed
done
kubectx $context2
kubectl apply -f deployment-r2.z1.yaml -n srs
copy_file ./bts.flv


kubectx $context1
kubectl apply -f deployment-r1.z1.yaml -n srs
copy_file ./source.flv 
kubectl apply -f ./ds-locality.yaml
kubectl apply -f ./srs-gateway.yaml

