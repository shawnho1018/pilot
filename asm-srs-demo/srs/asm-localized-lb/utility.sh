#!/bin/zsh
function play_stream {
    context=$1
    file="/usr/local/srs/bts.flv"
    if [[ $context == "gke_shawn-demo-2021_asia-east1-a_cluster-17" ]]; then
       file="/usr/local/srs/source.flv"
    fi
    pod=$(kubectl get pods -l app=srs -n srs --context $context | grep srs | awk '{print $1}')
    kubectl exec $pod -n srs --context $context -- objs/ffmpeg/bin/ffmpeg -re -i ${file} -c copy -f flv rtmp://localhost/live/livestream
}
function copy_file {
    context=$1
    file="./bts.flv"
    if [[ $context == "gke_shawn-demo-2021_asia-east1-a_cluster-17" ]]; then
       file="./source.flv"
    fi
  pod=$(kubectl get pods -l app=srs -n srs | grep srs | awk '{print $1}')
  kubectl wait --for=condition=ready pod ${pod} -n srs --context $context
  kubectl cp ${file} srs/$pod:/usr/local/srs -c srs --context $context
}