# Chaos Mesh Demo
This is a demo code for Chaos Mesh on GKE 1.19.10. It utilizes web-show to demonstrate the ping latency. [This doc](https://chaos-mesh.org/docs/user_guides/run_chaos_experiment) has all the details.

If you ever failed to install the CRDs from chaos-mesh with the docker version and applied your chaos experiment, your experiment is likely to stuck. Please kubectl edit your networkchaos object and remove the value in the "finalizer" tag and have the resource released. 
