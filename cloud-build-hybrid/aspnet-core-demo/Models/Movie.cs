﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MvcMovie.Models
{
    public class Movie
    {
        public int Id { get; set; }
        [Display(Name = "電影名稱")]
        public string Title { get; set; }

        [DataType(DataType.Date)]
        public DateTime ReleaseDate { get; set; }
        [Display(Name = "風格")]
        public string Genre { get; set; }
        [Display(Name = "價錢")]
        public decimal Price { get; set; }
    }
}