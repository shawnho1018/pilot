using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MvcMovie.Data;
using MvcMovie.Models;
using System;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.FileProviders.Physical;
using Microsoft.Extensions.Configuration;
//  Add GCP API Namesapces
using Google.Api.Gax.ResourceNames;
using Google.Cloud.SecretManager.V1;
using Google.Protobuf;

namespace MvcMovie
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;

                try
                {
                    SeedData.Initialize(services);
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred seeding the DB.");
                }
            }

            host.Run();

        }

        //  Michael: Modify below to accept secrets from Cloud Secret Manager
        public static string ReadFromSecretManager()
        {
            #region Michael : Demo Environment Variables vs ConfigMap/Secret
            string projectId = System.Environment.GetEnvironmentVariable("GCP_PROJECT_ID");
            string secretId = System.Environment.GetEnvironmentVariable("GCP_SM_APPSETTING_JSON");
            #endregion
            System.Console.WriteLine($"projectId=${projectId} | secretId=${secretId}");
            // Create the client.
            SecretManagerServiceClient client = SecretManagerServiceClient.Create();

            // Build the parent project name.
            ProjectName projectName = new ProjectName(projectId);

            var result = client.AccessSecretVersion($"projects/{projectId}/secrets/{secretId}/versions/1");
            if (result.Payload != null && result.Payload.CalculateSize() > 0)
            {
                string data = result.Payload.Data.ToStringUtf8();
                return data;
            }
            else
            {
                return "{}";
            }

        }
        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    //  Michael: Demonstrate how to specify listening ports, 
                    //          We are deploying to GKE, HTTPS will be handled by GCLB and we want our containers lintening on 8000
                    webBuilder.UseUrls("http://*:8080");
                })
                .ConfigureAppConfiguration(cfgBuilder =>
                {
                    cfgBuilder
                        .AddEnvironmentVariables()
                        .AddJsonStream(new System.IO.MemoryStream(System.Text.Encoding.UTF8.GetBytes(ReadFromSecretManager())))
                        .AddJsonFile("configs/appsettings.json", true, true)
                        .AddJsonFile("secrets/appsettings.json", true, true);
                });
        }
    }
}
