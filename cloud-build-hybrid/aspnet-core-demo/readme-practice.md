## Overview
In this document we will address several common practices when deploying your ASP.Net core application to GKE

## Istio

-   Install `istioctl` and `Egress Gateway`

```shell
curl -sL https://istio.io/downloadIstioctl | sh -
export PATH=$PATH:$HOME/.istioctl/bin

istioctl install -f ./k8s/0-enable-egress-gw.yaml
```

-   Instruct egress traffic from the application to external API go through Egress Gateway
```shell
kubectl apply -f ./k8s/external_service_serviceEntry.yaml
kubectl apply -f ./k8s/external_service_destinationRule.yaml 

```
## Gitlab Environments

-   Environments for `dev`, `staging` and `production`

    We want to divide our environments for different purposes, for example, we want to have a `dev` environemtn for developers to test their codes, this environment have least restriction, allows developers to deploy new bits frequently.

    We may also want to ahve a `staging` environemnt for QAs to verify functionalities, this environment has more restrictions than `dev` environment, may requires regular, scheduled deployment such as daily deployment.

    Finally we have `production` environment where we run our bits to serve exteranl users, this is the most restrictive environment, every deployment should be verified by QAs before deploying to `production` environment.

    In Gitlab, you can creaete new Environment via `Operations >> Environment`

    You can specify which environment to deploy in your `.gitlab-ci.yml` by setting `environment` value

```yaml
deploy_staging:
    stage: deploy
    only:
        - /^feature.*$/
    environment:
        name: Staging
        url: https://demo-staging-env.michaelchi.net
```

    You can also create variables that are only visible to particular environment so that in your pipeline you leverage different variables for deployment

## Configurations

Most ASP.Net core applications store application configuration in `appsettings.json` file, since we now have different environments, we want to manage different settings for different environemnts. We don't want to store `appsettings.json` in our source control to prevent unwanted access, we want to set those configuration values in a seperated secret store so that we can better manage access to those secrets.

While in the Cloud, we encourage developers to leverage services like Cloud Secret Manager to manage secrets.

- [Required Permissions to Access Cloud Secret Manager](https://cloud.google.com/secret-manager/docs/accessing-the-api)

- [Sample Codes](https://cloud.google.com/secret-manager/docs/creating-and-accessing-secrets#access)











```shell

istioctl install -f ./k8s/0-enable-egress-gw.yaml \
                   --set components.egressGateways.name=istio-egressgateway \
                   --set components.egressGateways.enabled=true


kubectl exec aspnetcore-854c59cd55-t9wf9 -c aspnetcore -- curl  -sL -o /dev/null -D - https://httpbin.org/headers
kubectl exec aspnetcore-854c59cd55-t9wf9  -c aspnetcore -- curl  -sL -o /dev/null -D - https://google.com
kubectl exec aspnetcore-854c59cd55-t9wf9 -c aspnetcore -- curl  -sL -o /dev/null -D - https://kalschi-demo-001.azurewebsites.net/api/demo001
kubectl exec aspnetcore-854c59cd55-t9wf9  -c aspnetcore -- curl https://kalschi-demo-001.azurewebsites.net/api/demo001

kubectl exec aspnetcore-854c59cd55-t9wf9  -c aspnetcore -- wget https://kalschi-demo-001.azurewebsites.net/api/demo001 
kubectl exec aspnetcore-854c59cd55-t9wf9  -c aspnetcore -- wget http://kalschi-demo-001.azurewebsites.net/api/demo001 
kubectl exec aspnetcore-854c59cd55-t9wf9  -c aspnetcore cat demo001.4
```