## Overview
In this document we are using Gitlab on GCP to demonstrate how to do CI/CD on GCP for ASP.Net core applications
For detailed Gitlab CI/CD check out Gitlab official document to [get started](https://gitlab.com/help/ci/quick_start/README#creating-a-gitlab-ciyml-file)


## Setup Runner
To leverage Gitlab for CI/CD on GCP, first we need to setup a runner to run our deployment jobs.
On GCP, provision a Linux machine and execute below commands on that machine to install required components.

### Install Docker

```shell
sudo apt-get update
sudo apt update
sudo apt install --yes apt-transport-https ca-certificates curl gnupg2 software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
sudo apt update
sudo apt install --yes docker-ce
```
### Install Gitlab Runner
Provision a new machine and run below command on it to install required components, you can reference to [this document](https://docs.gitlab.com/runner/install/linux-manually.html) or [this](https://gitlab.michaelchi.net/help/ci/runners/README.md#registering-a-specific-runner) for details

```shell
sudo apt --fix-broken install
sudo apt-get install git
curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb
sudo dpkg -i gitlab-runner_amd64.deb

sudo gitlab-runner start
```

### Register Runner

Once we have required components installed, next we will register this runner to Gitlab. Follow [detailed instruction](https://docs.gitlab.com/runner/register/) to register. Key steps are:

- Obtain Project registration token from `your gitlab >> settings >> CI/CD section`

- Obtain Gitlab URL from `your gitlab >> settings >> CI/CD section`

Once you have required Gitlab information, run below command in your runner machine
```shell
export YOUR_GITLAB_URL=https://someurl
export REGISTRATION_TOKEN=sometoken

sudo gitlab-runner register -n \
  --url $YOUR_GITLAB_URL \
  --registration-token $REGISTRATION_TOKEN \
  --executor docker \
  --description "My Docker Runner" \
  --docker-image "docker:19.03.12" \
  --docker-privileged \
  --docker-volumes "/certs/client"
```


## Create Pipeline

### Create Variables for Pipelines

To create a pipelie, add a new `.gitlab-ci.yml` in your application source root. Check [official document](https://gitlab.michaelchi.net/help/ci/yaml/README.md) for detailed yml file spec.

We have a sample pipelie defination file created, in which we uses environment variable to store certain secrets which should only be touched by administrators, such as IAM service account key. In our pipeline, we reference to these secrets by using variables. Some key secrets are:

- Google cloud platform Service Account Key file
  -  The service account will be used to run `docker` and `kubectl` commands, it must have sufficient permissions to push container images to `gcr.io` as well as connect to GKE clusters.

- [Reference](https://medium.com/@tasslin/%E9%9A%A8%E6%89%8B%E5%AF%AB%E5%AF%AB-gcp-5-gitlab-ci-gke-7c7b1c4e9eec)

### Sample Pipeline

Below is a sample pipeline, please check comments in the file to understand how it works

```yaml
# Default docker image will be using in the pipeline if no specific image specified in below stages
image: docker:19.03.12
variables:
  # This will instruct Docker not to start over TLS.
  DOCKER_TLS_CERTDIR: ""
  DOCKER_DRIVER: overlay2

services:
  - docker:19.03.12-dind

# stage defination
stages:
    - build
    - deploy
    
build_job:
    stage: build
    # This stage only be triggered in this branch
    only:
        - master
    # tags:
    #     - dev 
    before_script:
       - docker info
    # Scripts to run
    script:
        - ls
        - echo $IAM_ACCOUNT_KEY | docker login -u _json_key --password-stdin https://gcr.io
        - docker build -t $PRIVATE_REPO:$CI_PIPELINE_IID .
        - docker push $PRIVATE_REPO:$CI_PIPELINE_IID

deploy_job:
    stage: deploy
    only:
        - master
    # tags: 
    #     - dev 
    image: 
        name: google/cloud-sdk:latest
    before_script:
        - gcloud container clusters get-credentials $GKE_CLUSTER --zone $GKE_ZONE --project $GKE_PROJECT
    # the `sed...` line is critical since we want to replace the image with actual image tag we created in the pipeline
    script:
    - kubectl version
    - sed -i "s/CUSTOM_VERSION/$CI_PIPELINE_IID/g" ./k8s/deployment.yaml
    - kubectl apply -f ./k8s/deployment.yaml
    - kubectl apply -f ./k8s/service.yaml
    - kubectl apply -f ./k8s/gateway.yaml
    - kubectl apply -f ./k8s/virtualservice.yaml


```
## References

Here're some references specific for ASP.Net core applications

- [Managing appsettings.json](https://stackoverflow.com/questions/63170073/dotnet-core-3-1-how-to-use-addjsonfile-with-absolute-path-to-file?fbclid=IwAR1ivap3MhLUxpcd0acJU2JhIf03yO9RfefhqZ7dH4JJMVhOQvILJEhOcmo)

- [Managing ASP.Net Core appsettings.json in GKE](https://anthonychu.ca/post/aspnet-core-appsettings-secrets-kubernetes/)

### Create Secrets for appsettings.json

  - Run below command in your box to create a secret in GKE cluster

```shell
kubectl create secret generic secret-appsettings --from-file=./k8s/appsettings.json
```

  - In `deployment.yaml`, use below to mount the secret we created as a file

```yaml
                env:
                    - name: REDIS_ADDRESS
                      value: 10.29.175.171:6379
                    # - name: ASPNETCORE_URLS
                    #   value: http://*:5000
                volumeMounts:
                - name: secrets
                  mountPath: /app/configs
                  readOnly: true
            volumes:
            - name: secrets
              secret:
                secretName: secret-appsettings
```

  - To update Secret
```shell
kubectl create secret generic secret-appsettings \
    --from-file=./k8s/appsettings.json --dry-run -o yaml | kubectl apply -f -
```

  - To rolling restart PODs to apply new configuration
```shell
kubectl rollout restart deployment/aspnetcore
```

### Create ConfigMap for appsettings.json

- Create a ConfigMap for [appsettings.json](./k8s/appsettings.configMap.yaml)
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: appsettings-json
data:
  appsettings.json: |-
    {
        "Logging": {
          "LogLevel": {
            "Default": "Information",
            "Microsoft": "Warning",
            "Microsoft.Hosting.Lifetime": "Information"
          }
        },
        "AllowedHosts": "*",
        "API_URL": "https://kalschi-demo-001.azurewebsites.net/api/demo001"
      }
```

- Deploy the configMap to GKE cluster

```shell
kubectl apply -f ./k8s/appsettings.configMap.yaml
```

- Update [Deployment.yaml](./k8s/deployment-with-configmap.yaml) to reflect the change
```yaml
                volumeMounts:
                - name: appsettingd-volume
                  mountPath: /app/configs
                  readOnly: true
            volumes:
            - name: appsettingd-volume
              configMap:
                name: appsettings-json
```
## Workarunds

[Configure an exteranl services in Istio](https://github.com/istio/istio/issues/12390)
