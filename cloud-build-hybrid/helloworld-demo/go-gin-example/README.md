# Introduction
This project is to demonstrate golang gin, redis, and cloudsql. It mainly adapts from [this tutorial](https://github.com/eddycjy/go-gin-example/blob/master/models/models.go). Branch, chap2, is adapted from [this post](https://segmentfault.com/a/1190000013297683).
