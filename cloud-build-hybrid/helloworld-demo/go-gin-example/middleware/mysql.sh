#!/bin/zsh
instance_name="prod-instance"
gcloud sql instances create ${instance_name} --database-version=MYSQL_5_7 --cpu=2 --memory=8GiB --zone=asia-east1-a --root-password=password123
gcloud sql databases create -i ${instance_name} mdb