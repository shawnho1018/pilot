# Using Cloud Endpoints and Ingress for http/https connections
Remember to modify the PROJECT variable in and run ./install.sh. The healthcheck requires several minutes to be healthy. Please use the following command to check:
```
kubectl describe ingress nginx-ingress | grep ingress.kubernetes.io/backends

Annotations:                                  ingress.kubernetes.io/backends: {"k8s-be-30254--c0a65373b3f93633":"HEALTHY","k8s1-c0a65373-default-nginx-80-47d0879f":"HEALTHY"}
```
The results must be HEALTHY then you could try to use curl to connect to the FQDN 
```
curl -k https://nginx.endpoints.shawn-demo-2021.cloud.goog
```

