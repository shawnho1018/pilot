#!/bin/zsh
VIP_NAME="cep-ip"
PROJECT="shawn-demo-2021"
FQDN="nginx.endpoints.${PROJECT}.cloud.goog"
# Create SSH-KEY
openssl genrsa -out test-ingress-1.key 2048
openssl req -new -key test-ingress-1.key -out test-ingress-1.csr \
  -subj "/CN=${FQDN}"
openssl x509 -req -days 365 -in test-ingress-1.csr -signkey test-ingress-1.key \
  -out test-ingress-1.crt

kubectl create secret tls nginx \
  --cert test-ingress-1.crt --key test-ingress-1.key

# Create IP Address
gcloud compute addresses create ${VIP_NAME} --global
GIP=$(gcloud compute addresses list | grep cep-ip | awk '{print $2}')

cat <<EOF > dns-spec.yaml
swagger: "2.0"
info:
  description: "Cloud Endpoints DNS"
  title: "Cloud Endpoints DNS"
  version: "1.0.0"
paths: {}
host: ${FQDN}
x-google-endpoints:
- name: ${FQDN}
  target: "${GIP}"
EOF
#gcloud endpoints services undelete $FQDN
gcloud endpoints services deploy dns-spec.yaml

cat <<EOF > nginx-ingress.yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: nginx-ingress
  annotations:
    kubernetes.io/ingress.global-static-ip-name: ${VIP_NAME} 
  labels:
    name: nginx-ingress
spec:
  tls:
  - secretName: nginx
  rules:
  - host: ${FQDN}
    http:
      paths:
      - pathType: Prefix
        path: "/"
        backend:
          service:
            name: nginx
            port: 
              number: 80
EOF
kubectl apply -f nginx.yaml
kubectl apply -f backend.yaml
kubectl apply -f nginx-ingress.yaml
