#!/bin/zsh
kubectl delete -f nginx-ingress.yaml
kubectl delete -f backend.yaml
kubectl delete -f nginx.yaml
kubectl delete secret nginx
# Delete service has 30 days retention. Better keep it and re-deploy.
#gcloud endpoints services delete --quiet nginx.endpoints.shawn-demo-2021.cloud.goog
gcloud compute addresses delete --quiet cep-ip --global
