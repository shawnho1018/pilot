# PubSub Sample
## Step 1: Create Pub/Sub Topic 
```
gcloud pubsub topics create hello-user
```
## Step 2: Deploy CloudRun
```
PROJECT_ID="shawn-demo-2021"

gcloud builds submit --tag gcr.io/${PROJECT_ID}/pubsub
gcloud run deploy pubsub-tutorial --image gcr.io/${PROJECT_ID}/pubsub  --no-allow-unauthenticated
```
 
## Step 3: Create Subscription
```
PROJECT_ID="shawn-demo-2021"
# Create & Config ServiceAccount cloud-run-pubsub-invoker
gcloud iam service-accounts create cloud-run-pubsub-invoker --display-name "Cloud Run Pub/Sub Invoker" 
gcloud run services add-iam-policy-binding pubsub-tutorial --member=serviceAccount:cloud-run-pubsub-invoker@${PROJECT_ID}.iam.gserviceaccount.com --role=roles/run.invoker

# Subscribe topic 
gcloud pubsub subscriptions create myRunSubscription --topic hello-user --push-endpoint=https://pubsub-tutorial-z6b5qphocq-de.a.run.app/ --push-auth-service-account=cloud-run-pubsub-invoker@shawn-demo-2021.iam.gserviceaccount.com
```

## Step 4: Test pubsub
```
gcloud pubsub topics publish hello-user --message "Runner4" --attribute="imageName=abc"

# View the result from Cloudrun Log or command line
gcloud logging read "resource.type = "cloud_run_revision"\nresource.labels.service_name = "pubsub-tutorial"\nresource.labels.location = "asia-east1"\n severity>=DEFAULT"
```

