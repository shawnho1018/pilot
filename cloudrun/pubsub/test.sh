#!/bin/zsh
PROJECT_ID="shawn-demo-2021"

gcloud builds submit --tag gcr.io/${PROJECT_ID}/pubsub
gcloud run deploy pubsub-tutorial --image gcr.io/${PROJECT_ID}/pubsub  --no-allow-unauthenticated
gcloud pubsub topics publish hello-user --message "Runner4" --attribute="messageId=123"
