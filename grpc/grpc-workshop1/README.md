# Workshop1: GRPC with Istio
This workshop serves as a quick lab for customers who are interested in learning how layer-7 loadbalancer helps distributing GRPC workloads to different pods. Without layer-7 loadbalancer, all workloads will be centralized on one pod. In order to execute this workshop, please follow the instructions below:
## Pre-requisite
### Install GKE with ASM
Since this lab requires istio, please follow [this tutorial](https://gitlab.com/shawnho1018/pilot/-/tree/master/sre-workshop) to create your cluster. 

After the GKE cluster is created, please use retrieve your kube-context by 
```
gcloud container clusters get-credentials [your-cluster-name]
```

### Install Skaffold
We'll use skaffold to quickly guide you through the tutorial. Please install skaffold using [this guide](https://skaffold.dev/docs/install/)

### Install Stern (Optional)
We'll check the logs in different pods. You are welcome to use kubectl logs to probe each pod or you could install [stern](https://github.com/wercker/stern#installation). We'll use stern to show the results.

### Compile greeter_client
Set GOPATH equals to grpc-workshop1 folder. Our client binary will be created in $GOPATH/bin folder.
Enter greeter_client folder and run
```
go install 
```
You should see greeter-client binary in $GOPATH/bin.

## Lab-1: GRPC with K8S LoadBalancer
K8s Service provides an L4 loadBalancer for north-south connection. 

Switch the working directory to grpc-workshop1, simply press 
```
skaffold dev --profile k8s-lb
```
Check the corresponding loadbalancer ip using
```
grpc-workshop1 % kubectl get svc
NAME         TYPE           CLUSTER-IP    EXTERNAL-IP      PORT(S)          AGE
k8s-server   LoadBalancer   10.8.32.141   35.221.136.188   8051:31674/TCP   70s
```
Then, create two terminal windows (e.g. ctrl+d in iterm). In one of the windows, run greeter-client binary as below, where --addr should be the loadbalancer IP with port 8051 and --name is the hello username.
```
grpc-workshop1 % bin/greeter-client --addr 35.221.136.188:8051 --name shawn
2021/12/12 21:56:08 try to connect to server: 35.221.136.188:8051
2021/12/12 21:56:08 Dial done
2021/12/12 21:56:08 Greeting: Hello shawn
2021/12/12 21:56:09 Greeting: Hello shawn
2021/12/12 21:56:10 Greeting: Hello shawn
2021/12/12 21:56:11 Greeting: Hello shawn
2021/12/12 21:56:12 Greeting: Hello shawn
```

In the other window, run stern to view multiple all 3 pod replicas to see if our L4 loadbalancer evenly distribute the client's requests. You would see all requests were re-directed to only 1 pod instead of evenly distributed to 3 pods.

```
grpc-workshop1 % stern -t --selector=app=k8s-server
k8s-server-deployment-5f4f5b7f68-dskvt k8s-server 2021-12-12T22:05:55.369131572+08:00 2021/12/12 14:05:55 Received: shawn
k8s-server-deployment-5f4f5b7f68-dskvt k8s-server 2021-12-12T22:05:56.380637405+08:00 2021/12/12 14:05:56 Received: shawn
k8s-server-deployment-5f4f5b7f68-dskvt k8s-server 2021-12-12T22:05:57.393196446+08:00 2021/12/12 14:05:57 Received: shawn
k8s-server-deployment-5f4f5b7f68-dskvt k8s-server 2021-12-12T22:05:58.404470319+08:00 2021/12/12 14:05:58 Received: shawn
k8s-server-deployment-5f4f5b7f68-dskvt k8s-server 2021-12-12T22:05:59.416586513+08:00 2021/12/12 14:05:59 Received: shawn
k8s-server-deployment-5f4f5b7f68-dskvt k8s-server 2021-12-12T22:06:00.436945792+08:00 2021/12/12 14:06:00 Received: shawn
k8s-server-deployment-5f4f5b7f68-dskvt k8s-server 2021-12-12T22:06:01.452427641+08:00 2021/12/12 14:06:01 Received: shawn
k8s-server-deployment-5f4f5b7f68-dskvt k8s-server 2021-12-12T22:06:02.468674281+08:00 2021/12/12 14:06:02 Received: shawn
k8s-server-deployment-5f4f5b7f68-dskvt k8s-server 2021-12-12T22:06:03.480473466+08:00 2021/12/12 14:06:03 Received: shawn
k8s-server-deployment-5f4f5b7f68-dskvt k8s-server 2021-12-12T22:06:04.492741377+08:00 2021/12/12 14:06:04 Received: shawn
k8s-server-deployment-5f4f5b7f68-dskvt k8s-server 2021-12-12T22:06:05.504773755+08:00 2021/12/12 14:06:05 Received: shawn
k8s-server-deployment-5f4f5b7f68-dskvt k8s-server 2021-12-12T22:06:06.516598616+08:00 2021/12/12 14:06:06 Received: shawn
k8s-server-deployment-5f4f5b7f68-dskvt k8s-server 2021-12-12T22:06:07.536484284+08:00 2021/12/12 14:06:07 Received: shawn
k8s-server-deployment-5f4f5b7f68-dskvt k8s-server 2021-12-12T22:06:08.556773491+08:00 2021/12/12 14:06:08 Received: shawn
k8s-server-deployment-5f4f5b7f68-dskvt k8s-server 2021-12-12T22:06:09.569078973+08:00 2021/12/12 14:06:09 Received: shawn
k8s-server-deployment-5f4f5b7f68-dskvt k8s-server 2021-12-12T22:06:10.580607148+08:00 2021/12/12 14:06:10 Received: shawn
...
```
After you confirmed the results, press ctrl+c to stop skaffold dev. Then, we could continue our lab-2.

## Lab-2: GRPC with istio-ingressgateway
Instead of using K8s L4 Loadbalancer, let's switch gear to istio's L7 istio-ingressgateway. If you use managed ASM, this label could activate the sistio idecar auto-injection.
```
kubectl label ns [target-namespace-name] istio.io/rev=asm-managed
```
Then, we could start the deployment with the following command.
```
skaffold dev --profile istio
```
Since the north-south traffic is now provided by istio-ingressgateway, we need to first check its ip address before assigning server's ip to client. 
```
grpc-workshop1 % kubectl get svc -n istio-system
NAME                   TYPE           CLUSTER-IP    EXTERNAL-IP     PORT(S)                      AGE
istio-ingressgateway   LoadBalancer   10.8.42.123   35.201.146.96   80:32353/TCP,443:30811/TCP   12d
```
Again, let us run 
```
grpc-workshop1 % bin/greeter-client --addr 35.201.146.96:80 --name shawn
2021/12/12 22:14:21 try to connect to server: 35.201.146.96:80
2021/12/12 22:14:21 Dial done
2021/12/12 22:14:21 Greeting: Hello shawn
2021/12/12 22:14:22 Greeting: Hello shawn
2021/12/12 22:14:23 Greeting: Hello shawn
2021/12/12 22:14:24 Greeting: Hello shawn
```
The stern output looks like:
```
grpc-workshop1 % stern -t --selector=app=k8s-server
...
k8s-server-deployment-84c7b65d45-5l9zm k8s-server 2021-12-12T22:15:02.601665469+08:00 2021/12/12 14:15:02 Received: shawn
k8s-server-deployment-84c7b65d45-fv65c k8s-server 2021-12-12T22:15:03.613518669+08:00 2021/12/12 14:15:03 Received: shawn
k8s-server-deployment-84c7b65d45-9x52s k8s-server 2021-12-12T22:15:04.629342506+08:00 2021/12/12 14:15:04 Received: shawn
k8s-server-deployment-84c7b65d45-5l9zm k8s-server 2021-12-12T22:15:05.641561183+08:00 2021/12/12 14:15:05 Received: shawn
k8s-server-deployment-84c7b65d45-fv65c k8s-server 2021-12-12T22:15:06.653169184+08:00 2021/12/12 14:15:06 Received: shawn
k8s-server-deployment-84c7b65d45-9x52s k8s-server 2021-12-12T22:15:07.665519502+08:00 2021/12/12 14:15:07 Received: shawn
k8s-server-deployment-84c7b65d45-5l9zm k8s-server 2021-12-12T22:15:08.685416735+08:00 2021/12/12 14:15:08 Received: shawn
```
The result shows clearly that istio-ingressgateway could distribute the client workloads to all the pod replicas.

## Lab-3: GRPC with GatewayAPI (Failed to Support GRPC)
You may heard that there is going to be a new Ingress to replace the existing K8s Ingress. Unlike the existing ingress, this new ingress could redirect requests to pod in other namespace. It also provides matching capability to hostname, subpath, and request header info. Let's see how to utilize this new GatewayAPI in GRPC.

### Install GatewayClass CRD
Please make sure your GKE cluster is not located in the following regions where GatewayAPI [was not supported](https://cloud.google.com/kubernetes-engine/docs/how-to/deploying-gateways) yet.
* us-west3
* northamerica-northeast2
* europe-west6
* europe-central2
* australia-southeast2
* asia-southeast2
* asia-south1
* asia-south2
* asia-northeast3

### Deploy GatewayAPI CRD
GKE cluster requires to input GatewayAPI CRDs for Gateway Controller. The following command could be used to deploy the required CRDs.
```
kubectl kustomize "github.com/kubernetes-sigs/gateway-api/config/crd?ref=v0.3.0" | kubectl apply -f -
```
Wait 10~15 minutes, you should see the following GatewayClass appeared in the cluster.
```
grpc-workshop1 % kubectl get gatewayclass
NAME          CONTROLLER                  AGE
gke-l7-gxlb   networking.gke.io/gateway   14m
gke-l7-rilb   networking.gke.io/gateway   14m
```

### Test GatewayAPI
The latest supported gatewayapi is v1alpha1 in GKE. According to [the user manual](https://cloud.google.com/kubernetes-engine/docs/how-to/gatewayclass-capabilities), GRPC is not supported yet. To better demonstrate gatewayapi, I use an nginx deployment instead. Please type the following command to learn how the gatewayapi works.
```
skaffold dev --profile=gatewayapi
```
Since gatewayapi utilized google's loadbalancer, it took 3~5 minutes to have the urlmap and backend working. You could check the resulting gateway and httproute as shown below:
```
grpc-workshop1 % kubectl describe gtw
Name:         external-http
Namespace:    webrtc
Labels:       skaffold.dev/run-id=110aaf1a-12be-4cc6-8fdd-f275a6250983
  ...
Status:
  Addresses:
    Type:   IPAddress
    Value:  34.149.246.20
  ...
Events:
  Type     Reason  Age                   From                   Message
  ----     ------  ----                  ----                   -------
  Normal   ADD     58m                   sc-gateway-controller  webrtc/external-http
  Warning  SYNC    48m                   sc-gateway-controller  failed to translate Gateway "webrtc/external-http": Error GWCER105: Listener is invalid, err: unsupported protocol "GRPC".
  Normal   SYNC    11m (x3 over 56m)     sc-gateway-controller  webrtc/external-http
  Normal   UPDATE  7m38s (x8 over 58m)   sc-gateway-controller  webrtc/external-http
  Normal   SYNC    3m43s (x15 over 57m)  sc-gateway-controller  SYNC on webrtc/external-http was a success

grpc-workshop1 % kubectl describe httproute
Name:         grpc-server
Namespace:    webrtc
Labels:       gateway=external-http
              skaffold.dev/run-id=110aaf1a-12be-4cc6-8fdd-f275a6250983
...
Events:
  Type    Reason  Age                 From                   Message
  ----    ------  ----                ----                   -------
  Normal  ADD     59m                 sc-gateway-controller  webrtc/grpc-server
  Normal  UPDATE  13m (x3 over 57m)   sc-gateway-controller  webrtc/grpc-server
  Normal  SYNC    62s (x15 over 56m)  sc-gateway-controller  Bind of HTTPRoute "webrtc/grpc-server" to Gateway "webrtc/external-http" was a success
  Normal  SYNC    62s (x15 over 56m)  sc-gateway-controller  Reconciliation of HTTPRoute "webrtc/grpc-server" bound to Gateway "webrtc/external-http" was a success
```
By describing gtw object, we could see the created gclb ip address as 34.149.246.20. We could test with curl to see gatewayapi works with HTTP protocol.
```
grpc-workshop1 % curl 34.149.246.20
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```