/*
 *
 * Copyright 2015 gRPC authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

// Package main implements a client for Greeter service.
package main

import (
	"context"
	"log"
	"os"
	"time"
	"google.golang.org/grpc"
	pb "greeter-client/helloworld"
	"google.golang.org/grpc/metadata"	
)

const (
	address     = "35.194.240.254:80"
	defaultName = "world"
	action      = "Go to school"
)
func callShowResponse(c pb.GreeterClient) {
	// Contact the server and print out its response.
	name := defaultName
	if len(os.Args) > 1 {
		name = os.Args[1]
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	var header metadata.MD	
	r, err := c.SayHello(ctx, &pb.HelloRequest{Name: name}, grpc.Header(&header))
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	hostname := "unknown"
	// [START istio_sample_apps_grpc_greeter_go_client_hostname]
	if len(header["hostname"]) > 0 {
		hostname = header["hostname"][0]
	}
	log.Printf("%s from %s with Greeting %s", r.Message, hostname, r.GetMessage())
	// [END istio_sample_apps_grpc_greeter_go_client_hostname]


	// Send Goodbye
	request := action
	r1, err1 := c.SayGoodbye(ctx, &pb.HelloRequest{Name: request}, grpc.Header(&header))
	if err1 != nil {
		log.Fatalf("could not goodbye: %v", err1)
	}
	// [START istio_sample_apps_grpc_greeter_go_client_hostname]
	if len(header["hostname"]) > 0 {
		hostname = header["hostname"][0]
	}
	log.Printf("%s from %s with Goodbye %s", r.Message, hostname, r1.GetMessage())
	// [END istio_sample_apps_grpc_greeter_go_client_hostname]	
}
func main() {
	// Set up a connection to the server.
	log.Printf("enter client")
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	log.Printf("Dial done")
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewGreeterClient(conn)
    for {
		callShowResponse(c)
		time.Sleep(5 * time.Second)
	}
}
