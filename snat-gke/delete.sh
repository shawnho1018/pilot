#!/bin/zsh
gws=( $(gcloud compute instances list | grep natgw | awk '{print $1}') )
for gw in ${gws[@]}; do
  gcloud compute routes delete --quiet ${gw}
done

gcloud compute instance-groups managed delete --quiet natgw
gcloud compute health-checks delete --quiet nat-health-check
gcloud compute instance-templates delete --quiet natgw
