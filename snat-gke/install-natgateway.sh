#!/bin/zsh
gcloud compute instance-templates create natgw \
--machine-type n1-standard-2 --can-ip-forward --tags natgw --network private-cluster-1-network --subnet private-cluster-1-subnetwork --metadata=startup-script-url=gs://shawn-demo-2021/nat-startup.sh

gcloud compute health-checks create http nat-health-check --check-interval 30 \
    --healthy-threshold 1 --unhealthy-threshold 3 --timeout 5s --port 80 --request-path /

gcloud compute instance-groups managed create natgw --size=2 --template=natgw --zone=asia-east1-a
gcloud beta compute instance-groups managed set-autohealing natgw \
    --health-check nat-health-check --initial-delay 120 --zone asia-east1-a

gws=( $(gcloud compute instances list | grep natgw | awk '{print $1}') )
for gw in $gws[@]; do
  gcloud compute routes create nat-route-${gw} --network private-cluster-1-network \
    --destination-range 104.16.0.0/16 \
    --next-hop-instance $gw \
    --next-hop-instance-zone asia-east1-a \
    --tags no-ip --priority 900
done
