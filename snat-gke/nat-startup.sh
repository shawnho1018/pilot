#!/bin/bash
export DEBIAN_FRONTEND=noninteractive
apt-get -y update
apt-get install -y debconf-utils
echo iptables-persistent iptables-persistent/autosave_v4 boolean true | sudo debconf-set-selections
apt-get -y install iptables-persistent

sysctl -w net.ipv4.ip_forward=1
sed -i= 's/^[# ]*net.ipv4.ip_forward=[[:digit:]]/net.ipv4.ip_forward=1/g' /etc/sysctl.conf
iptables -t nat -A POSTROUTING -o ens4 -j MASQUERADE
iptables-save

# Create a web-server for healthcheck
sudo apt update && sudo apt -y install apache2
echo '<!doctype html><html><body><h1>Hello World!</h1></body></html>' | sudo tee /var/www/html/index.html
