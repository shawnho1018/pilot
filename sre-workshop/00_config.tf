terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.75.0"
    }

    google-beta = {
      source = "hashicorp/google-beta"
      version = "3.75.0"
    }
  }
  backend "gcs" {
    bucket  = "shawn-demo-2021-tf"
    prefix  = "terraform/state"
  }
}

provider "google" {
  project = var.project_id
  region  = var.region
}

provider "google-beta" {
  project = var.project_id
  region  = var.region
}
