module "gke" {
  source          = "./01_gke"
  cluster_name    = var.cluster_name
  location        = var.zones[0]
  project_id      = data.google_project.shawn-demo-2021.project_id
  vm_type         = var.vm_type
}