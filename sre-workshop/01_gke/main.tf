### Kubernetes clusters
resource "google_container_cluster" "primary" {
  provider = google-beta
  project  = var.project_id

  # Here's how you specify the name
  name = var.cluster_name
  ip_allocation_policy {
    
  }
  # Set the zone by grabbing the result of the random_shuffle above. It
  # returns a list so we have to pull the first element off. If you're looking
  # at this and thinking "huh terraform syntax looks a clunky" you are NOT WRONG
  location = var.location
  networking_mode = "VPC_NATIVE"

  # Enable Workload Identity for cluster
  workload_identity_config {
    identity_namespace = "${var.project_id}.svc.id.goog"
  }

  node_pool {
    node_config {
      machine_type = var.vm_type

      oauth_scopes = [
        "https://www.googleapis.com/auth/cloud-platform"
      ]

      labels = {
        environment = "dev",
        cluster     = var.cluster_name
      }

      # Enable Workload Identity for node pool
      workload_metadata_config {
        node_metadata = "GKE_METADATA_SERVER"
      }
    }

    initial_node_count = 2

    autoscaling {
      min_node_count = 2
      max_node_count = 4
    }

    management {
      auto_repair  = true
      auto_upgrade = true
    }
  }
  # Specifies the use of "new" Cloud logging and monitoring
  # https://cloud.google.com/kubernetes-engine-monitoring/
  logging_service    = "logging.googleapis.com/kubernetes"
  monitoring_service = "monitoring.googleapis.com/kubernetes"
  addons_config {
    horizontal_pod_autoscaling {
      disabled = true
    }
  }    
}
