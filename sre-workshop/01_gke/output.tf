output "endpoint" {
    value = google_container_cluster.primary.endpoint
}
output "location" {
    value = google_container_cluster.primary.location
}
output "name" {
    value = google_container_cluster.primary.name
}