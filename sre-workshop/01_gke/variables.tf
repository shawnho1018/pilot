variable "cluster_name" {
  type        = string
  description = "cluster name"
}

variable "location" {
  type        = string
  description = "GCP region name where GKE cluster should be deployed."
}

variable "project_id" {
  type        = string
  default     = "shawn-demo-2021"
}
variable "vm_type" {
  type        = string
  default     = "n1-standard-4"
}