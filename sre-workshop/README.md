# Create GKE with ASM
This terraform project is to create a GKE cluster with ASM component installed. Please check variable.tf and create your own .tfvars file as the sample in shawn.tfvars. After your tfvars is created, simply run the following command:
```
terraform init
terraform apply -var-file [your tfvars file] -auto-approve
```

Note: the existing terraform module suffers this issue in managed ASM control plane. If you see the following error: [Cluster Membership Error](https://cloud.google.com/service-mesh/docs/managed/troubleshoot#cluster_membership_error_no_identity_provider_specified), please run this command
```
gcloud container hub memberships register --enable-workload-identity
```

