variable "project_id" {
  type = string 
  description = "Project to store terraform state variables"
}
variable "region" {
  type = string
}
variable "zones" {
  description = "The primary zones to be used"
  default = ["asia-east1-a"]
}
variable "cluster_name" {
  description = "cluster name used in the lab"
}
variable "cluster_service_account_id" {
  description = "" # TODO
}

variable "vm_type" {
  default = "n1-standard-4"
}
