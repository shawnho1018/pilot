locals {
  key_content = file(var.gcp_key_path)
}

provider "google" {
  project = var.project_id
  region  = var.primary_region
  credentials = local.key_content
}
# we also use the random provider so let's pin that too
provider "random" {
  version = "~> 2.0"
}
# provider project_id
data "google_client_config" "current" {
}
# provider project_id
data "google_project" "project" {
  project_id = var.project_id
}

module "project-services" {
  source  = "terraform-google-modules/project-factory/google//modules/project_services"
  project_id  = data.google_project.project.project_id
  disable_services_on_destroy = false
  activate_apis = [
    "anthos.googleapis.com",
    "container.googleapis.com",    
    "cloudscheduler.googleapis.com",
    "clouddebugger.googleapis.com",
    "cloudresourcemanager.googleapis.com",    
    "clouderrorreporting.googleapis.com",   
    "cloudtrace.googleapis.com",    
    "compute.googleapis.com",
    "gkeconnect.googleapis.com",
    "gkehub.googleapis.com",    
    "iam.googleapis.com",
    "iamcredentials.googleapis.com",
    "meshca.googleapis.com",
    "meshtelemetry.googleapis.com",
    "meshconfig.googleapis.com",
    "monitoring.googleapis.com",
    "logging.googleapis.com",
    "stackdriver.googleapis.com"
  ]
}