module "gke" {
  source          = "./01_gke"
  cluster_name    = var.cluster_name
  location        = var.primary_zones[0]
  project_id      = var.project_id
}