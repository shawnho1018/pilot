provider "google" {
  # pin provider to 3.23.0
  version = ">=3.23.0"
  project = var.project_id
} 
### Kubernetes clusters
resource "google_container_cluster" "primary" {
  provider = google-beta
  project  = var.project_id

  # Here's how you specify the name
  name = var.cluster_name

  # Set the zone by grabbing the result of the random_shuffle above. It
  # returns a list so we have to pull the first element off. If you're looking
  # at this and thinking "huh terraform syntax looks a clunky" you are NOT WRONG
  location = var.location
  networking_mode = "VPC_NATIVE"
  ip_allocation_policy {
    
  }
  # Enable Workload Identity for cluster
  workload_identity_config {
    identity_namespace = "${var.project_id}.svc.id.goog"
  }

  # Using an embedded resource to define the node pool. Another
  # option would be to create the node pool as a separate resource and link it
  # to this cluster. There are tradeoffs to each approach.
  
  # The embedded resource is convenient but if you change it you have to tear
  # down the entire cluster and rebuild it. A separate resource could be
  # modified independent of the cluster without the cluster needing to be torn
  # down.
  #
  # For this particular case we're not going to be modifying the node pool once
  # it's deployed, so it makes sense to accept the tradeoff for the convenience
  # of having it inline.
  #
  # Many of the parameters below are self-explanatory so I'll only call out
  # interesting things.
  node_pool {
    node_config {
      machine_type = "n1-standard-4"

      oauth_scopes = [
        "https://www.googleapis.com/auth/cloud-platform"
      ]

      labels = {
        environment = "dev",
        cluster     = var.cluster_name
      }

      # Enable Workload Identity for node pool
      workload_metadata_config {
        node_metadata = "GKE_METADATA_SERVER"
      }
    }

    initial_node_count = 2

    autoscaling {
      min_node_count = 2
      max_node_count = 10
    }

    management {
      auto_repair  = true
      auto_upgrade = true
    }
  }

  # Specifies the use of "new" Cloud logging and monitoring
  # https://cloud.google.com/kubernetes-engine-monitoring/
  logging_service    = "logging.googleapis.com/kubernetes"
  monitoring_service = "monitoring.googleapis.com/kubernetes"
  addons_config {
    horizontal_pod_autoscaling {
      disabled = true
    }
    cloudrun_config {
      disabled=false
    }
  }    
}
