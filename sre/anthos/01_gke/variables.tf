variable "cluster_name" {
  type        = string
  description = "cluster name"
}

variable "location" {
  type        = string
  default     = "asia-east1-a"
  description = "GCP region name where GKE cluster should be deployed."
}

variable "project_id" {
  type        = string
  default     = "shawn-demo-2021"
}