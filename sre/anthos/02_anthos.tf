module "hub-primary" {
  source           = "terraform-google-modules/kubernetes-engine/google//modules/hub"
  project_id       = data.google_project.project.project_id
  cluster_name     = var.cluster_name
  location         = var.primary_zones[0]
  cluster_endpoint = module.gke.endpoint
  gke_hub_membership_name = var.cluster_name
  use_existing_sa = true
  sa_private_key  = base64encode(local.key_content)
  depends_on = [module.gke.endpoint]
}

module "asm-primary" {
  source           = "terraform-google-modules/kubernetes-engine/google//modules/asm"
  project_id       = data.google_project.project.project_id
  cluster_name     = var.cluster_name
  asm_version      = 1.8
  location         = var.primary_zones[0]
  cluster_endpoint = module.gke.endpoint
  asm_dir          = "asm-${var.cluster_name}"
  depends_on = [module.hub-primary.wait]
}

module "acm-primary" {
  source           = "github.com/terraform-google-modules/terraform-google-kubernetes-engine//modules/acm"
  project_id       = data.google_client_config.current.project
  cluster_name     = var.cluster_name
  location         = var.primary_zones[0]
  cluster_endpoint = module.gke.endpoint

  operator_path    = "scripts/config-management-operator.yaml"
  sync_repo        = var.acm_repo_location
  sync_branch      = var.acm_branch
  ssh_auth_key     = file("${path.root}/${var.ssh_auth_key_path}")

  enable_policy_controller = true
  depends_on = [module.asm-primary.asm_wait]
}

module "app-workload-identity" {
  source     = "terraform-google-modules/kubernetes-engine/google//modules/workload-identity"
  name       = "terraform-admin"
  namespace  = "default"
  project_id = var.project_id
  roles = ["roles/owner", "roles/storage.admin"]
  depends_on = [module.acm-primary.wait]
}

module "istio" {
  source     = "./02_anthos"
}