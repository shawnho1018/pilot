#!/bin/zsh
set -e

if [ "$#" -lt 4 ]; then
    >&2 echo "Not all expected arguments set."
    exit 1
fi

PROJECT_ID=$1

CLUSTER_NAME=$2
CLUSTER_LOCATION=$3
ASM_VERSION=$4
MANAGED=$5

MODE="install"

# Download the correct version of the install_asm script
curl https://storage.googleapis.com/csm-artifacts/asm/install_asm_"${ASM_VERSION}" > install_asm
chmod u+x install_asm

declare -a params=(
    "--verbose"
    "--project_id ${PROJECT_ID}"
    "--cluster_name ${CLUSTER_NAME}"
    "--cluster_location ${CLUSTER_LOCATION}"
    "--mode ${MODE}"
    "--enable_cluster_labels"
    "--enable_cluster_roles"
    "--enable_gcp_apis"
    "--enable_gcp_components"
    "--enable_cluster_labels"
)

# Add the --managed param if MANAGED is set to true
if [[ "${MANAGED}" == true ]]; then
    params+=("--managed")
fi

# Run the script with appropriate flags
echo "Running ./install_asm" "${params[@]}"

# Disable shell linting. Other forms will prevent the command to work
# shellcheck disable=SC2046,SC2116
./install_asm $(echo "${params[@]}")