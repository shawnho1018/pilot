module "ratingservice" {
  source          = "./03_ratingservice"
  gcp_project_id  = data.google_project.project.project_id
  gcp_region_name = "us-central1"
}