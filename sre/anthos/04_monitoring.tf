module "service_monitoring" {
  source          = "./04_monitoring"
  project_id      = data.google_project.project.project_id
  project_number  = data.google_project.project.number
  external_ip     = module.istio.external-ip
}