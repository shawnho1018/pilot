# Here we create a default HTTP Uptime Check on the External IP of our
# Kubernetes cluster revealed by the Load Balancer
# Our uptime check has a period of 1 minute and times out after 10 seconds
resource "google_monitoring_uptime_check_config" "http" {
  display_name = "HTTP Uptime Check"
  timeout      = "10s"
  period       = "60s"  

  http_check {
    path = "/"
    port = "80"
  }

  monitored_resource {
    type = "uptime_url"
    labels = {
      project_id = "${var.project_id}"
      host       = "${var.external_ip}"
    }
  }
}

# Creates a dashboard for the general user experience - it contains metrics
# that reflect how users are interacting with the demo application such as latency
# of requests, distribution of types of requests, and response types. The JSON object
# containing the exact details of the dashboard can be found in the 'dashboards' folder.
resource "google_monitoring_dashboard" "userexp_dashboard" {
  dashboard_json = file("${path.module}/dashboards/userexp_dashboard.json")
}

# Creates a dashboard for the frontend service. The details of the charts
# in the dashboard can be found in the JSON specification file.
resource "google_monitoring_dashboard" "frontend_dashboard" {
  dashboard_json = file("${path.module}/dashboards/frontend_dashboard.json")
}

# Creates a dashboard for the adservice. The details of the charts
# in the dashboard can be found in the JSON specification file.
resource "google_monitoring_dashboard" "adservice_dashboard" {
  dashboard_json = file("${path.module}/dashboards/adservice_dashboard.json")
}

# Creates a dashboard for the recommendationservice. The details of the charts
# in the dashboard can be found in the JSON specification file.
resource "google_monitoring_dashboard" "recommendationservice_dashboard" {
  dashboard_json = file("${path.module}/dashboards/recommendationservice_dashboard.json")
}

# Creates a dashboard for the cartservice.
resource "google_monitoring_dashboard" "cartservice_dashboard" {
  dashboard_json = file("${path.module}/dashboards/cartservice_dashboard.json")
}

# Creates a dashboard for the checkoutservice.
resource "google_monitoring_dashboard" "checkoutservice_dashboard" {
  dashboard_json = file("${path.module}/dashboards/checkoutservice_dashboard.json")
}

# Creates a dashboard for the currencyservice.
resource "google_monitoring_dashboard" "currencyservice_dashboard" {
  dashboard_json = file("${path.module}/dashboards/currencyservice_dashboard.json")
}

# Creates a dashboard for the productcatalogservice.
resource "google_monitoring_dashboard" "productcatalogservice_dashboard" {
  dashboard_json = file("${path.module}/dashboards/productcatalogservice_dashboard.json")
}

# Creates a dashboard for the ratingservice.
resource "google_monitoring_dashboard" "ratingservice_dashboard" {
  dashboard_json = file("${path.module}/dashboards/ratingservice_dashboard.json")
}
# We need the service name for dashboard and chart titles, and we need the service ID to use as a metadata filter.
variable "services" {
  type = list(object({
    service_name = string,
    service_id   = string
  }))
  default = [
    {
      service_name = "Payment Service"
      service_id   = "paymentservice"
    },
    {
      service_name = "Email Service"
      service_id   = "emailservice"
    },
    {
      service_name = "Shipping Service"
      service_id   = "shippingservice"
    }
  ]
}

# Iterate over the services that we defined and create a dashboard template file for each one using
# the template file defined in the 'dashboards' folder.
data "template_file" "dash_json" {
  template = file("${path.module}/dashboards/generic_dashboard.tmpl")
  count    = length(var.services)
  vars = {
    service_name = var.services[count.index].service_name
    service_id   = var.services[count.index].service_id
  }
}

# Create GCP Monitoring Dashboards using the rendered template files that were created in the data
# resource above. This produces one dashboard for each microservice that we defined above.
resource "google_monitoring_dashboard" "service_dashboards" {
  count          = length(var.services)
  dashboard_json = <<EOF
${data.template_file.dash_json[count.index].rendered}
EOF
}
# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Define specifics for each of the services that will receive SLOs through a custom service
# The data members needed are to specify the custom service, goals for each SLO (availability and latency),
# and maximum error budget burn rates.
variable "custom_services" {
  type = list(object({
    service_name = string,
    service_id = string,
    availability_goal = number,
    availability_burn_rate = number,
    latency_goal = number,
    latency_threshold = number,
    latency_burn_rate = number
  }))
  default = [
    {
      service_name = "Frontend Service"
      service_id = "frontend"
      availability_goal = 0.9		# configurable goal for the availability SLO (0.9 = 90% of requests are successsful)
      availability_burn_rate = 2	# limit on error budget burn rate (2 indicates we alert if error budget is consumed 2x faster than it should)
      latency_goal = 0.9			# configurable goal for the latency SLO (0.9 = 90% of requests finish in under the latency threshold)
      latency_threshold = 500		# indicates 500ms as the maximum latency of a 'good' request
      latency_burn_rate = 2
    },
    {
      service_name = "Checkout Service"
      service_id = "checkoutservice"
      availability_goal = 0.99
      availability_burn_rate = 2
      latency_goal = 0.99
      latency_threshold = 500
      latency_burn_rate = 2
    },
    {
      service_name = "Payment Service"
      service_id = "paymentservice"
      availability_goal = 0.99
      availability_burn_rate = 2
      latency_goal = 0.99
      latency_threshold = 500
      latency_burn_rate = 2
    },
    {
      service_name = "Email Service"
      service_id = "emailservice"
      availability_goal = 0.99
      availability_burn_rate = 2
      latency_goal = 0.99
      latency_threshold = 500
      latency_burn_rate = 2
    },
    {
      service_name = "Shipping Service"
      service_id = "shippingservice"
      availability_goal = 0.99
      availability_burn_rate = 2
      latency_goal = 0.99
      latency_threshold = 500
      latency_burn_rate = 2
    }
  ]
}

# Create a custom service that we attach our SLOs to
# Using a custom service here allows us to add on additional SLOs and 
# alerting policies on things such as custom metrics.
#
# There is the option to use an Istio service since Istio automatically detects and creates 
# services for us. This example uses a custom service to demonstrate Terraform support for 
# creating custom services with attached SLOs and alerting policies.
resource "google_monitoring_custom_service" "custom_service" {
  count = length(var.custom_services)
  service_id = "${var.custom_services[count.index].service_id}-srv"
  display_name = var.custom_services[count.index].service_name
}

# Specify services that will use the Istio service that is automatically detected
# and created by installing Istio on the Kubernetes cluster.
# The data members required are to successfully set up SLOs (goals and latency thresholds)
# and burn rate limits for alerting policies
variable "istio_services" {
  type = list(object({
    service_name = string,
    service_id = string,
    availability_goal = number,
    availability_burn_rate = number,
    latency_goal = number,
    latency_threshold = number,
    latency_burn_rate = number
  }))
  default = [
    {
      service_name = "Cart Service"
      service_id = "cartservice"
      availability_goal = 0.99
      availability_burn_rate = 2
      latency_goal = 0.99
      latency_threshold = 500
      latency_burn_rate = 2
    },
    {
      service_name = "Product Catalog Service"
      service_id = "productcatalogservice"
      availability_goal = 0.99
      availability_burn_rate = 2
      latency_goal = 0.99
      latency_threshold = 500
      latency_burn_rate = 2
    },
    {
      service_name = "Currency Service"
      service_id = "currencyservice"
      availability_goal = 0.99
      availability_burn_rate = 2
      latency_goal = 0.99
      latency_threshold = 500
      latency_burn_rate = 2
    },
    {
      service_name = "Recommendation Service"
      service_id = "recommendationservice"
      availability_goal = 0.99
      availability_burn_rate = 2
      latency_goal = 0.99
      latency_threshold = 500
      latency_burn_rate = 2
    },
    {
      service_name = "Ad Service"
      service_id = "adservice"
      availability_goal = 0.99
      availability_burn_rate = 2
      latency_goal = 0.99
      latency_threshold = 500
      latency_burn_rate = 2
    }
  ]
}

# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Create an SLO for availability for the custom service.
# Example SLO is defined as following:
#   90% of HTTP requests are successful within the past 30 day windowed period

resource "google_monitoring_slo" "custom_service_availability_slo" {
  count        = length(var.custom_services)
  service      = google_monitoring_custom_service.custom_service[count.index].service_id
  slo_id       = "availability-slo"
  display_name = "Availability SLO with request base SLI (good total ratio)"

  # The goal sets our objective for successful requests over the 30 day rolling window period
  goal                = var.custom_services[count.index].availability_goal
  rolling_period_days = 30

  request_based_sli {
    good_total_ratio {

      # The "good" service is the number of 200 OK responses
      good_service_filter = join(" AND ", [
        "metric.type=\"istio.io/service/server/request_count\"",
        "resource.type=\"k8s_container\"",
        "resource.label.\"cluster_name\"=\"cluster-primary\"",
        "metadata.user_labels.\"app\"=\"${var.custom_services[count.index].service_id}\"",
        "metric.label.\"response_code\"=\"200\""
      ])

      # The total is the number of non-4XX responses
      # We eliminate 4XX responses since they do not accurately represent server-side 
      # failures and have the possibility of skewing our SLO measurements
      total_service_filter = join(" AND ", [
        "metric.type=\"istio.io/service/server/request_count\"",
        "resource.type=\"k8s_container\"",
        "resource.label.\"cluster_name\"=\"cluster-primary\"",
        "metadata.user_labels.\"app\"=\"${var.custom_services[count.index].service_id}\"",
        join(" OR ", ["metric.label.\"response_code\"<\"400\"",
        "metric.label.\"response_code\">=\"500\""])
      ])

    }
  }
}

# Create another SLO on the custom service this time with respect to latency.
# Example SLO is defined as following:
#   90% of requests return in under 500 ms in the previous 30 days
resource "google_monitoring_slo" "custom_service_latency_slo" {
  count        = length(var.custom_services)
  service      = google_monitoring_custom_service.custom_service[count.index].service_id
  slo_id       = "latency-slo"
  display_name = "Latency SLO with request base SLI (distribution cut)"

  goal                = var.custom_services[count.index].latency_goal
  rolling_period_days = 30

  request_based_sli {
    distribution_cut {

      # The distribution filter retrieves latencies of requests that returned 200 OK responses
      distribution_filter = join(" AND ", [
        "metric.type=\"istio.io/service/server/response_latencies\"",
        "resource.type=\"k8s_container\"",
        "resource.label.\"cluster_name\"=\"cluster-primary\"",
        "metric.label.\"response_code\"=\"200\"",
        "metadata.user_labels.\"app\"=\"${var.custom_services[count.index].service_id}\""
      ])

      range {

        # By not setting a min value, it is automatically set to -infinity
        # The units of the upper bound is in ms
        max = var.custom_services[count.index].latency_threshold

      }
    }
  }
}

# Create an SLO for availability for the Istio service.
# Example SLO is defined as following:
#   90% of HTTP requests are successful within the past 30 day windowed period
resource "google_monitoring_slo" "istio_service_availability_slo" {
  count = length(var.istio_services)

  # Uses the Istio service that is automatically detected and created by installing Istio
  # Identify the service using the string: ist:${project_id}-zone-${zone}-cloud-ops-sandbox-default-${service_id}
  service      = "canonical-ist:proj-${var.project_number}-default-${var.istio_services[count.index].service_id}"
  slo_id       = "availability-slo"
  display_name = "Availability SLO with request base SLI (good total ratio)"

  # The goal sets our objective for successful requests over the 30 day rolling window period
  goal                = var.istio_services[count.index].availability_goal
  rolling_period_days = 30

  request_based_sli {
    good_total_ratio {

      # The "good" service is the number of 200 OK responses
      good_service_filter = join(" AND ", [
        "metric.type=\"istio.io/service/server/request_count\"",
        "resource.type=\"k8s_container\"",
        "resource.label.\"cluster_name\"=\"cloud-ops-sandbox\"",
        "metadata.user_labels.\"app\"=\"${var.istio_services[count.index].service_id}\"",
        "metric.label.\"response_code\"=\"200\""
      ])

      # The total is the number of non-4XX responses
      # We eliminate 4XX responses since they do not accurately represent server-side 
      # failures and have the possibility of skewing our SLO measurements
      total_service_filter = join(" AND ", [
        "metric.type=\"istio.io/service/server/request_count\"",
        "resource.type=\"k8s_container\"",
        "resource.label.\"cluster_name\"=\"cluster-primary\"",
        "metadata.user_labels.\"app\"=\"${var.istio_services[count.index].service_id}\"",
        join(" OR ", ["metric.label.\"response_code\"<\"400\"",
        "metric.label.\"response_code\">=\"500\""])
      ])

    }
  }
}

# Create an SLO with respect to latency using the Istio service.
# Example SLO is defined as:
#   99% of requests return in under 500 ms in the previous 30 days
resource "google_monitoring_slo" "istio_service_latency_slo" {
  count        = length(var.istio_services)
  service      = "canonical-ist:proj-${var.project_number}-default-${var.istio_services[count.index].service_id}"
  slo_id       = "latency-slo"
  display_name = "Latency SLO with request base SLI (distribution cut)"

  goal                = var.istio_services[count.index].latency_goal
  rolling_period_days = 30

  request_based_sli {
    distribution_cut {

      # The distribution filter retrieves latencies of requests that returned 200 OK responses
      distribution_filter = join(" AND ", [
        "metric.type=\"istio.io/service/server/response_latencies\"",
        "resource.type=\"k8s_container\"",
        "resource.label.\"cluster_name\"=\"cluster-primary\"",
        "metric.label.\"response_code\"=\"200\"",
        "metadata.user_labels.\"app\"=\"${var.istio_services[count.index].service_id}\""
      ])

      range {
        # By not setting a min value, it is automatically set to -infinity
        # The upper bound for latency is in ms
        max = var.istio_services[count.index].latency_threshold

      }
    }
  }
}
# Rating service availability SLO:
#   99% of HTTP requests are successful within the past 30 day windowed period

resource "google_monitoring_slo" "rating_service_availability_slo" {
  # Uses ratingservice service that is automatically detected and created when the service is deployed to App Engine
  # Identify of the service is built after the following template: gae:${project_id}_servicename
  service      = "gae:${var.project_id}_ratingservice"
  slo_id       = "availability-slo"
  display_name = "Availability SLO with request base SLI (good total ratio)"

  # The goal sets our objective for successful requests over the 30 day rolling window period
  goal                = 0.99
  rolling_period_days = 30

  request_based_sli {
    good_total_ratio {

      # The "good" service is the number of 200 OK responses
      good_service_filter = join(" AND ", [
        "metric.type=\"appengine.googleapis.com/http/server/response_count\"",
        "resource.type=\"gae_app\"",
        "resource.label.\"version_id\"=\"prod\"",
        "resource.label.\"module_id\"=\"ratingservice\"",
        "metric.label.\"loading\"=\"false\"",
        "metric.label.\"response_code\"=\"200\""
      ])

      # The total is the number of non-4XX and 429 (Too Many Requests) responses
      # We eliminate 4XX responses except 429 (Too Many Requests) since they do not accurately represent server-side 
      # failures and have the possibility of skewing our SLO measurements
      total_service_filter = join(" AND ", [
        "metric.type=\"appengine.googleapis.com/http/server/response_count\"",
        "resource.type=\"gae_app\"",
        "resource.label.\"version_id\"=\"prod\"",
        "resource.label.\"module_id\"=\"ratingservice\"",
        "metric.label.\"loading\"=\"false\"",
        # a mix of 'AND' and 'OR' operators is not currently supported
        # without the following filter responses with HTTP status [400,500) will be included
        # while a general guideline to exclude them since they do not reflect service issues
        # join(" OR ", ["metric.label.\"response_code\"<\"400\"",
        #   "metric.label.\"response_code\"=\"429\"",
        # "metric.label.\"response_code\">=\"500\""])
      ])
    }
  }
}

# Rating service latency SLO:
#   99% of requests that return in under 175 ms in the previous 30 days

resource "google_monitoring_slo" "rating_service_latency_slo" {
  # Uses ratingservice service that is automatically detected and created when the service is deployed to App Engine
  # Identify of the service is built after the following template: gae:${project_id}_servicename
  service      = "gae:${var.project_id}_ratingservice"
  slo_id       = "latency-slo"
  display_name = "Latency SLO with request base SLI (distribution cut)"

  goal                = 0.99
  rolling_period_days = 30

  request_based_sli {
    distribution_cut {

      # The distribution filter retrieves latencies of user requests that returned 200 OK responses
      distribution_filter = join(" AND ", [
        "metric.type=\"appengine.googleapis.com/http/server/response_latencies\"",
        "resource.type=\"gae_app\"",
        "resource.label.\"version_id\"=\"prod\"",
        "resource.label.\"module_id\"=\"ratingservice\"",
        "metric.label.\"loading\"=\"false\"",
        "metric.label.\"response_code\"=\"200\"",
      ])

      range {
        # By not setting a min value, it is automatically set to -infinity
        # The upper bound for latency is in ms
        max = 500
      }
    }
  }
}

# Rating service's data freshness SLO:
# during a day 99.9% of minutes have at least 1 successful recollect API call
resource "google_monitoring_slo" "rating_service_freshness_slo" {
  # Uses ratingservice service that is automatically detected and created when the service is deployed to App Engine
  # Identify of the service is built after the following template: gae:${project_id}_servicename
  service      = "gae:${var.project_id}_ratingservice"
  slo_id       = "freshness-slo"
  display_name = "Rating freshness SLO with window based SLI"

  goal                = 0.99
  rolling_period_days = 1

  windows_based_sli {
    window_period = "60s"
    metric_sum_in_range {
      time_series = join(" AND ", [
        "metric.type=\"logging.googleapis.com/user/ratingservice_recollect_requests_count\"",
        "resource.type=\"gae_app\"",
        join(" OR ", ["metric.label.\"status\"<\"400\"",
        "metric.label.\"status\"=\"429\""])
      ])
      range {
        min = 1
        max = 9999 # the maximum can be any number of requests; the unreasonably high value is placed since inf is not supported
      }
    }
  }

  depends_on = [google_logging_metric.ratingservice_logging_metric]
}
# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Create an alerting policy on the Availability SLO for the custom service.
# The definition of the SLO can be found in the file '03_service_slo.tf'.
# We alert on budget burn rate, alerting if burn rate exceeds the threshold defined for the service
resource "google_monitoring_alert_policy" "custom_service_availability_slo_alert" {
  count = length(var.custom_services)
  display_name = "${var.custom_services[count.index].service_name} Availability Alert Policy"
  combiner     = "AND"
  conditions {
    display_name = "SLO burn rate alert for availability SLO with a threshold of ${var.custom_services[count.index].availability_burn_rate}"
    condition_threshold {

      # This filter alerts on burn rate over the past 60 minutes
      filter     = "select_slo_burn_rate(\"projects/${var.project_id}/services/${google_monitoring_custom_service.custom_service[count.index].service_id}/serviceLevelObjectives/${google_monitoring_slo.custom_service_availability_slo[count.index].slo_id}\", 60m)"
      
      # The threshhold is determined by how quickly we burn through our error budget.
      # Example: if threshold_value = 2 then error budget is consumed in 15 days.
      # Details: https://landing.google.com/sre/workbook/chapters/alerting-on-slos/#4-alert-on-burn-rate 
      threshold_value = var.custom_services[count.index].availability_burn_rate
      comparison = "COMPARISON_GT"
      duration   = "60s"
    }
  }
  documentation {
    content = "Availability SLO burn for the ${var.custom_services[count.index].service_name} for the past 60m exceeded ${var.custom_services[count.index].availability_burn_rate}x the acceptable budget burn rate. The service is returning less OK responses than desired. Consider viewing the service logs or custom dashboard to retrieve more information or adjust the values for the SLO and error budget."
    mime_type = "text/markdown"
  }
}

# Create another alerting policy, this time on the SLO for latency for the custom service.
# Alert on budget burn rate as well.
resource "google_monitoring_alert_policy" "custom_service_latency_slo_alert" {
  count = length(var.custom_services)
  display_name = "${var.custom_services[count.index].service_name} Latency Alert Policy"
  combiner     = "AND"
  conditions {
    display_name = "SLO burn rate alert for latency SLO with a threshold of ${var.custom_services[count.index].latency_burn_rate}"
    condition_threshold {
      filter     = "select_slo_burn_rate(\"projects/${var.project_id}/services/${google_monitoring_custom_service.custom_service[count.index].service_id}/serviceLevelObjectives/${google_monitoring_slo.custom_service_latency_slo[count.index].slo_id}\", 60m)"
      threshold_value = var.custom_services[count.index].availability_burn_rate
      comparison = "COMPARISON_GT"
      duration   = "60s"
    }
  }
  documentation {
    content = "Latency SLO burn for the ${var.custom_services[count.index].service_name} for the past 60m exceeded ${var.custom_services[count.index].latency_burn_rate}x the acceptable budget burn rate. The service is responding slower than desired. Consider viewing the service logs or custom dashboard to retrieve more information or adjust the values for the SLO and error budget."
    mime_type = "text/markdown"
  }
}

# Create an alerting policy on the Availability SLO for the Istio service.
# The definition of the SLO can be found in the file '04_slos.tf'.
# Alerts on error budget burn rate.
resource "google_monitoring_alert_policy" "istio_service_availability_slo_alert" {
  count = length(var.istio_services)
  display_name = "${var.istio_services[count.index].service_name} Availability Alert Policy"
  combiner     = "AND"
  conditions {
    display_name = "SLO burn rate alert for availability SLO with a threshold of ${var.istio_services[count.index].availability_burn_rate}"
    condition_threshold {

      # This filter alerts on burn rate over the past 60 minutes
      # The service is defined by the unique Istio string that is automatically created
      filter     = "select_slo_burn_rate(\"projects/${var.project_id}/services/canonical-ist:proj-${var.project_number}-default-${var.istio_services[count.index].service_id}/serviceLevelObjectives/${google_monitoring_slo.istio_service_availability_slo[count.index].slo_id}\", 60m)"
      threshold_value = var.istio_services[count.index].availability_burn_rate
      comparison = "COMPARISON_GT"
      duration   = "60s"
    }
  }
  documentation {
    content = "Availability SLO burn for the ${var.istio_services[count.index].service_name} for the past 60m exceeded ${var.istio_services[count.index].availability_burn_rate}x the acceptable budget burn rate. The service is returning less OK responses than desired. Consider viewing the service logs or custom dashboard to retrieve more information or adjust the values for the SLO and error budget."
    mime_type = "text/markdown"
  }
}

# Create another alerting policy, this time on the SLO for latency for the Istio service.
# Alerts on error budget burn rate.
resource "google_monitoring_alert_policy" "istio_service_latency_slo_alert" {
  count = length(var.istio_services)
  display_name = "${var.istio_services[count.index].service_name} Latency Alert Policy"
  combiner     = "AND"
  conditions {
    display_name = "SLO burn rate alert for latency SLO with a threshold of ${var.istio_services[count.index].latency_burn_rate}"
    condition_threshold {
      filter     = "select_slo_burn_rate(\"projects/${var.project_id}/services/canonical-ist:proj-${var.project_number}-default-${var.istio_services[count.index].service_id}/serviceLevelObjectives/${google_monitoring_slo.istio_service_latency_slo[count.index].slo_id}\", 60m)"
      threshold_value = var.istio_services[count.index].availability_burn_rate
      comparison = "COMPARISON_GT"
      duration   = "60s"
    }
  }
  documentation {
    content = "Latency SLO burn for the ${var.istio_services[count.index].service_name} for the past 60m exceeded ${var.istio_services[count.index].latency_burn_rate}x the acceptable budget burn rate. The service is responding slower than desired. Consider viewing the service logs or custom dashboard to retrieve more information or adjust the values for the SLO and error budget."
    mime_type = "text/markdown"
  }
}
# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Creates a log-based metric by extracting a specific log written by the Checkout Service. 
# The log being used for the metric is from the Checkout Service and has the format:
# orderedItem="Vintage Typewriter", id="OLJCESPC7Z"
#
# The label and Regex extractor are used to create filters on the metric
# This resource creates only the metric
resource "google_logging_metric" "checkoutservice_logging_metric" {
  name   = "checkoutservice_log_metric"
  filter = "resource.type=k8s_container AND resource.labels.cluster_name=cluster-primary AND resource.labels.namespace_name=default AND resource.labels.container_name=server AND orderedItem"
  metric_descriptor {
    metric_kind = "DELTA" # set to DELTA for counter-based metric
    value_type  = "INT64" # set to INT64 for counter-based metric
    unit        = "1"
    labels {
      key         = "product_name"
      description = "Filters by Product Name"
    }
    labels {
      key         = "product_id"
      description = "Filters by Product Id"
    }
    display_name = "Ordered Products Metric"
  }
  label_extractors = {
    # Regex extractor has matching group to match the product name or product id. Example: orderedItem="Terrarium", id="L9ECAV7KIM" 
    # matches Terrarium for product name and L9ECAV7KIM for product id.
    "product_name" = "REGEXP_EXTRACT(jsonPayload.message, \"orderedItem=\\\\\\\"([^\\\"]+)\\\\\\\"\")"
    "product_id"   = "REGEXP_EXTRACT(jsonPayload.message, \"id=\\\\\\\"([^\\\"]+)\\\\\\\"\")"
  }
}

# Creates a dashboard and chart for the log-based metric defined above.
# Uses the label to group by the product name
resource "google_monitoring_dashboard" "log_based_metric_dashboard" {
  dashboard_json = <<EOF
{
  "displayName": "Log Based Metric Dashboard",
  "gridLayout": {
    "columns": "2",
    "widgets": [
      {
        "title": "Number of Products Ordered per day grouped by Product Name",
        "xyChart": {
          "dataSets": [
            {
              "timeSeriesQuery": {
                "timeSeriesFilter": {
                  "filter": "metric.type=\"logging.googleapis.com/user/${google_logging_metric.checkoutservice_logging_metric.name}\" resource.type=\"k8s_container\"",
                  "aggregation": {
                    "perSeriesAligner": "ALIGN_SUM",
                    "crossSeriesReducer": "REDUCE_MEAN",
                    "groupByFields": [
                      "metric.label.\"product_name\""
                    ]
                  }
                }
              },
              "plotType": "LINE",
              "minAlignmentPeriod": "86400s"
            }
          ],
          "yAxis": {
            "label": "y1Axis",
            "scale": "LINEAR"
          },
          "chartOptions": {
            "mode": "COLOR"
          }
        }
      }
    ]
  }
}
EOF
}

resource "google_logging_metric" "ratingservice_logging_metric" {
  name   = "ratingservice_recollect_requests_count"
  filter = "resource.type=gae_app AND resource.labels.module_id=ratingservice AND resource.labels.version_id=prod protoPayload.method=POST AND protoPayload.resource=\"/ratings:recollect\""
  metric_descriptor {
    metric_kind = "DELTA" # set to DELTA for counter-based metric
    value_type  = "INT64" # set to INT64 for counter-based metric
    unit        = "1"
    labels {
      key         = "status"
      value_type  = "INT64"
      description = "Response status"
    }
    display_name = "Number of recollect requests to rating service"
  }
  label_extractors = {
    "status" = "EXTRACT(protoPayload.status)"
  }
}

