variable "project_id" {
  default     = "shawn-demo-2021"
  type        = string
  description = "Project ID"
}
variable "project_number" {
  type        = string
}
variable "external_ip" {
  default     = "34.80.215.43"
  type        = string
}
variable "zone" {
  default     = "asia-east1-a"
}