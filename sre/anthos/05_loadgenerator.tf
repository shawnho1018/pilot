module loadgenerator {
  source           = "./05_loadgenerator"
  cluster_name    = var.cluster_name
  location        = var.primary_zones[0]
  project_id      = var.project_id
  external_ip     = "${module.istio.external-ip}"
}