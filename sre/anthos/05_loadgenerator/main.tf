
# Create Load_generator for blackbox testing
resource "null_resource" "set_env_vars" {
  provisioner "local-exec" {
    command = "kubectl create configmap address-config --from-literal=FRONTEND_ADDR=http://${var.external_ip}"
  }
}
module "load_generator" {
  source = "terraform-google-modules/gcloud/google//modules/kubectl-wrapper"
  project_id              = var.project_id
  cluster_name            = var.cluster_name
  cluster_location        = var.location
  use_existing_context    = true
  enabled                 = true
  kubectl_create_command  = "kubectl apply -f ${path.module}/../../loadgenerator-manifests"
  kubectl_destroy_command = "kubectl delete -f ${path.module}/../../loadgenerator-manifests"
  module_depends_on       = [null_resource.set_env_vars]
}