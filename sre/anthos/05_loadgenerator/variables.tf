variable "project_id" {
  default     = "shawn-demo-2021"
  type        = string
  description = "Project ID"
}
variable "cluster_name" {
  default     = "cluster-primary"
  description = "Anthos cluster name to install LoadGenerator"
}
variable "location" {
  default     = "asia-east1-a"
}
variable "external_ip" {
  default     = "34.80.215.43"
  type        = string
}