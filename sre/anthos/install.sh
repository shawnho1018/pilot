#!/bin/bash
function message() {
  msg=$1
  echo "****************************"
  echo "$1"
  echo "Press any key to continue..."
  echo "****************************"
  read -n 1
}
source ~/.bashrc
terraform version
echo "Please first prepare a github for ACM and a Service account key"
echo "Modify the corresponding variable.tf"
message "Enable Anthos Services"
terraform apply -auto-approve -target module.project-services

message "Create GKE cluster"
terraform apply -auto-approve -target module.gke

message "Register GKE to Anthos Hub"
terraform apply -auto-approve -target module.hub-primary

message "Install ASM 1.8"
terraform apply -auto-approve -target module.asm-primary

message "Install ACM 1.8"
terraform apply -auto-approve -target module.acm-primary

message "Download Demo Scripts"
gcloud source repos clone config-repo --project=shawn-demo-2021
git clone https://gitlab.com/shawnho1018/anthos-demo.git
