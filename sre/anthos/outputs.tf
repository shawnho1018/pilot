output "project" {
  value = data.google_client_config.current.project
}
output "project_number" {
  value = data.google_project.project.number
}
output "ratingservice_ip" {
  value = module.ratingservice.service_url
}
output "istio_external_ip" {
  value = module.istio.external-ip
}
output "db_user" {
  value = "postgres"
}
output "db_password" {
  value = module.ratingservice.db_password
}
output "db_instance" {
  value = module.ratingservice.db_instance
}
output "db_host" {
  value = module.ratingservice.db_host
}