#!/bin/bash
export PATH=/home/shawnho/software:$PATH
terraform version
function message() {
  msg=$1
  echo "****************************"
  echo "$1"
  echo "Press any key to continue..."
  echo "****************************"
  read -n 1
}
echo "We are going to remove Anthos cluster"

message "Delete ACM 1.8"
terraform destroy -auto-approve -target module.acm-primary

message "Delete ASM 1.8"
terraform destroy -auto-approve -target module.asm-primary

message "Unregister GKE to Anthos Hub"
terraform destroy -auto-approve -target module.hub-primary

message "Delete GKE cluster"
terraform destroy -auto-approve -target module.gke
