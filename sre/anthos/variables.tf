variable "project_id" {
  description = "The project ID to host the cluster in"
  default = "shawn-demo-2021"
}
variable "cluster_name" {
  description = "Name of the cluster"
  default = "cluster-sre"
}
variable "primary_region" {
  description = "The primary region to be used"
  default = "us-central1"
}
variable "primary_zones" {
  description = "The primary zones to be used"
  default = ["us-central1-a"]
}

variable "acm_repo_location" {
  description = "The location of the git repo ACM will sync to"
  default = "ssh://shawnho@google.com@source.developers.google.com:2022/p/shawn-demo-2021/r/config-repo"
}
variable "acm_branch" {
  description = "The git branch ACM will sync to"
  default = "master"
}
variable "ssh_auth_key_path" {
  description = "SSH-key path for git repo access"
  default = "keys/repo.key"
}

variable "gcp_key_path" {
  description = "Existing GCP service account key path"
  default = "/Users/shawnho/workspace/gcp-keys/shawn-demo-2021-sa.key"
}