module "gke" {
  source                     = "terraform-google-modules/kubernetes-engine/google//modules/private-cluster"
  project_id                 = var.project_id
  name                       = var.cluster_name
  region                     = var.primary_region
  zones                      = var.primary_zones
  network                    = var.vpc
  subnetwork                 = var.subnet
  ip_range_pods              = var.secondary-iprange-pods
  ip_range_services          = var.secondary-iprange-services
  http_load_balancing        = true
  horizontal_pod_autoscaling = true
  network_policy             = true
  regional                   = false
  enable_private_endpoint    = false
  enable_private_nodes       = true
  master_ipv4_cidr_block     = "192.168.2.16/28"
  master_authorized_networks = [
    {
      cidr_block   = "0.0.0.0/0"
      display_name = "allow all IPs to access the master"
    }
  ]
  node_pools = [
    {
      name               = "my-pool"
      machine_type       = "n1-standard-4"
      min_count          = 1
      max_count          = 5
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS_CONTAINERD"
      auto_repair        = true
      auto_upgrade       = true
      service_account    = "anthos-install@wlouis-vpc-sc-lab.iam.gserviceaccount.com"
      preemptible        = false
      initial_node_count = 2
    },
  ]

  node_pools_labels = {
    all = {}

    my-pool = {
     default-pool  = "true"
    }
  }

  node_pools_metadata = {
    all = {}

    my-pool = {
      node-pool-metadata-custom-value = "my-node-pool"
    }
  }

  node_pools_taints = {
    all = []

    my-pool = [
      {
        key    = "my-node-pool"
        value  = "true"
        effect = "PREFER_NO_SCHEDULE"
      },
    ]
  }

  node_pools_tags = {
    all = []

    default-node-pool = [
      "default-node-pool",
    ]
  }
}
