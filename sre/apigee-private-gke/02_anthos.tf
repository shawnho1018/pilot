module "hub-primary" {
  source           = "terraform-google-modules/kubernetes-engine/google//modules/hub"
  project_id       = var.project_id
  cluster_name     = var.cluster_name
  location         = var.primary_zones[0]
  cluster_endpoint = module.gke.endpoint
  gke_hub_membership_name = var.cluster_name
  use_existing_sa = true
  sa_private_key  = base64encode(local.key_content)
}
resource "null_resource" "install-asm" {
  provisioner "local-exec" {
    command = <<-EOT
      #gsutil cp gs://csm-artifacts/asm/install_asm_1.8 ./install_asm
      #chmod +x ./install_asm
      #mkdir -p ./asm_install
      ./install_asm -v --project_id ${var.project_id} --cluster_name ${var.cluster_name} --cluster_location ${var.primary_zones[0]} -s anthos-install@wlouis-vpc-sc-lab.iam.gserviceaccount.com -k ${var.gcp_key_path} --mode install --output-dir asm_install --enable_cluster_labels --enable_cluster_roles
      kubectl patch svc istio-ingressgateway -n istio-system --type="json" -p '[{"op": "add", "path": "/spec/loadBalancerIP", "value": "${var.loadBalancerIP}"]'
    EOT
  }
}

module "app-workload-identity" {
  source     = "terraform-google-modules/kubernetes-engine/google//modules/workload-identity"
  name       = "terraform-admin"
  namespace  = "default"
  project_id = var.project_id
  roles = ["roles/owner", "roles/storage.admin"]
}
