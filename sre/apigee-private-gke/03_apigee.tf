module "apigee-services" {
  source  = "terraform-google-modules/project-factory/google//modules/project_services"
  project_id  = var.project_id
  disable_services_on_destroy = false
  activate_apis = [
    "anthos.googleapis.com",
    "apigee.googleapis.com",
    "apigeeconnect.googleapis.com",
    "pubsub.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "compute.googleapis.com",
    "container.googleapis.com",
  ]
}

module "cert-manager" {
  source = "terraform-google-modules/gcloud/google//modules/kubectl-wrapper"
  project_id              = var.project_id
  cluster_name            = var.cluster_name
  cluster_location        = var.primary_zones[0]
  enabled                 = true
  kubectl_create_command  = "kubectl apply  -f ./cert-manager.yaml"
  kubectl_destroy_command = "kubectl delete -f ./cert-manager.yaml"
}

resource "null_resource" "signed_cert" {
  provisioner "local-exec" {
    command = "${path.module}/scripts/gen-cert.sh"
    interpreter = ["/bin/bash"]
    environment = {
        ACCOUNT_EMAIL    = var.cert_user_email
        CLOUDDNS_PROJECT = var.dns_project_id
        DOMAIN_NAME      = var.domain_name
        CERT_KEY_PATH    = var.gcp_key_clouddns_path
        CERT_KEY         = var.gcp_key_clouddns_name
    }
  }
  provisioner "local-exec" {
    when    = destroy
    command = <<-EOT
      echo "run destroy command......................................................"
      kubectl delete secret cert-admin -n istio-system
      kubectl delete -f ./cert-issuer.yaml
    EOT
  }
}
resource "null_resource" "download-apigee" {
  provisioner "local-exec" {
      command = "${path.module}/scripts/download-apigee.sh"
      interpreter = ["/bin/bash"]
      environment = {
        VERSION        = var.apigee_version
        APIGEE_PROJECT = var.project_id
    }  
  }
  provisioner "local-exec" {
    when   = destroy
    command = <<-EOT
      echo "Delete apigee service account"
      gcloud iam service-accounts delete --quiet apigee-org-admin@wlouis-vpc-sc-lab.iam.gserviceaccount.com
      gcloud iam service-accounts delete --quiet apigee-synchronizer@wlouis-vpc-sc-lab.iam.gserviceaccount.com
      gcloud iam service-accounts delete --quiet apigee-udca@wlouis-vpc-sc-lab.iam.gserviceaccount.com
      gcloud iam service-accounts delete --quiet apigee-cassandra@wlouis-vpc-sc-lab.iam.gserviceaccount.com
      gcloud iam service-accounts delete --quiet apigee-logger@wlouis-vpc-sc-lab.iam.gserviceaccount.com
      gcloud iam service-accounts delete --quiet apigee-mart@wlouis-vpc-sc-lab.iam.gserviceaccount.com
      gcloud iam service-accounts delete --quiet apigee-metrics@wlouis-vpc-sc-lab.iam.gserviceaccount.com
      gcloud iam service-accounts delete --quiet apigee-watcher@wlouis-vpc-sc-lab.iam.gserviceaccount.com
      gcloud iam service-accounts delete --quiet apigee-distributed-trace@wlouis-vpc-sc-lab.iam.gserviceaccount.com
      rm -rf hybrid-files/
      rm -rf apigeectl/
    EOT
  }
}

resource "null_resource" "install-apigee" {
  provisioner "local-exec" {
      command = "${path.module}/scripts/install-apigee.sh"
      interpreter = ["/bin/bash"]
      environment = {
        APIGEE_REGION  = var.apigee_region
        APIGEE_PROJECT = var.project_id
        REGION         = var.primary_region
        CLUSTER_NAME   = var.cluster_name
        VIRTUALHOST_NAME = var.virtualhost_name
        ENV              = var.apigee_env
    }
  }
  provisioner "local-exec" {
    when   = destroy
    command = <<-EOT
      echo "Delete apigee"
      cd hybrid-files
      ../apigeectl/apigeectl delete -f overrides/overrides-apigee.yaml
    EOT
  }  
}
