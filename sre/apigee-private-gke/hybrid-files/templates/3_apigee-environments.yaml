{{- $runtimeName := "apigee-runtime" -}}
{{- $synchronizerName := "apigee-synchronizer" -}}
{{- $udcaName := "apigee-udca" -}}
{{- $udcaTlsReadinessName := "apigee-tls-readiness" -}}
{{- $authzName := "apigee-authz" -}}
{{- $fluentdName := "apigee-fluentd" -}}
{{- $schemaReadinessName := "apigee-cassandra-schema-readiness" -}}
{{- if $.Org -}}
{{- $seenOrgEnvs := makeBoolMap }}
{{- range $index, $env := .Environments }}
{{- $generatedName := EnvScopeEncodedName $.Org $env.Name -}}
{{- $orgEnv := (printf "%s::%s" $.Org $env.Name) }}
{{- if not (index $seenOrgEnvs $orgEnv) }}
{{- $_ := addBoolMapEntry $seenOrgEnvs $orgEnv }}
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: {{ $runtimeName }}-{{ $generatedName }}-sa
  namespace: {{ $.Namespace }}
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: {{ $synchronizerName }}-{{ $generatedName }}-sa
  namespace: {{ $.Namespace }}
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: {{ $udcaName }}-{{ $generatedName }}-sa
  namespace: {{ $.Namespace }}
{{- if ne $.GCP.WorkloadIdentityEnabled "true" }}
---
apiVersion: v1
kind: Secret
metadata:
  name: "{{ $synchronizerName }}-{{ $generatedName }}-svc-account"
  namespace: {{ $.Namespace }}
type: Opaque
data:
  client_secret.json: |
    {{ loadSynchronizerServiceAccount $.GCP.ProjectID $.ContractProvider $.Org $env.Name (loadFile "Synchronizer Service Account" $env.ServiceAccountPaths.Synchronizer) $.ValidateServiceAccounts }}
---
apiVersion: v1
kind: Secret
metadata:
  name: "{{ $udcaName }}-{{ $generatedName }}-svc-account"
  namespace: {{ $.Namespace }}
type: Opaque
data:
  client_secret.json: |
    {{ loadUDCAServiceAccount $.GCP.ProjectID $.ContractProvider $.Org $env.Name (loadFile "UDCA Service Account" $env.ServiceAccountPaths.UDCA) $.ValidateServiceAccounts }}
---
{{- if $env.ServiceAccountPaths.Runtime }}
apiVersion: v1
kind: Secret
metadata:
  name: "{{ $runtimeName }}-{{ $generatedName }}-svc-account"
  namespace: {{ $.Namespace }}
type: Opaque
data:
  client_secret.json: |
    {{ loadMartServiceAccount (loadFile "Runtime Service Account" $env.ServiceAccountPaths.Runtime) }}
---
{{- end }}
{{- end }}
apiVersion: v1
kind: Secret
metadata:
  name: "{{ $generatedName }}-encryption-keys"
  namespace: {{ $.Namespace }}
type: Opaque
data:
  kmsEncryptionKey: |
  {{- if $.KMSEncryptionPath }}
    {{ loadFileForSecret $.KMSEncryptionPath }}
  {{- else if $.KMSEncryptionKey }}
    {{ validateEncryptionKey $.KMSEncryptionKey "KMS (org level)" }}
  {{ else }}
    {{ validateEncryptionKey $.Defaults.OrganizationDefaults.KMSEncryptionKey "KMS (default org level)" }}
  {{- end }}
  cacheEncryptionKey: |
  {{- if $env.CacheEncryptionPath }}
    {{ loadFileForSecret $env.CacheEncryptionPath }}
  {{- else if $env.CacheEncryptionKey }}
    {{ validateEncryptionKey $env.CacheEncryptionKey "Cache (env level)" }}
  {{ else }}
    {{ validateEncryptionKey $.Defaults.EnvironmentDefaults.CacheEncryptionKey "Cache (default env level)" }}
  {{- end }}
  envKvmEncryptionKey: |
  {{- if $env.KVMEncryptionPath }}
    {{ loadFileForSecret $env.KVMEncryptionPath }}
  {{- else if $.KVMEncryptionKey }}
  {{/* KVM encryption key with env scope.*/}}
    {{ validateEncryptionKey $env.KVMEncryptionKey "KVM (env level)" }}
  {{ else }}
    {{ validateEncryptionKey $.Defaults.EnvironmentDefaults.KVMEncryptionKey "KVM (default env level)" }}
  {{- end }}
  kvmEncryptionKey: |
  {{- if $.KVMEncryptionPath }}
    {{ loadFileForSecret $.kDMEncryptionPath }}
  {{- else if $.KVMEncryptionKey }}
  {{/* KVM encryption key with org scope.*/}}
    {{ validateEncryptionKey $.KVMEncryptionKey "KVM (org level)" }}
  {{ else }}
    {{ validateEncryptionKey $.Defaults.OrganizationDefaults.KVMEncryptionKey "KVM (default org level)" }}
  {{- end }}
---
apiVersion: apigee.cloud.google.com/v1alpha1
kind: ApigeeEnvironment
metadata:
  name: {{ $generatedName }}
  namespace: {{ $.Namespace }}
release:
  forceUpdate: true
spec:
  {{- if and (gt (len $.ImagePullSecrets) 0) (index $.ImagePullSecrets 0).Name }}
  imagePullSecrets:
    {{- range $index, $element := $.ImagePullSecrets }}
    - name: {{ $element.Name }}
    {{- end }}
  {{- end }}
  name: {{ $env.Name }}
  organizationRef: {{ OrgScopeEncodedName $.Org }}
  components:
    synchronizer:
      podServiceAccountName: {{ $synchronizerName }}-{{ $generatedName }}-sa
      {{- if ne $.GCP.WorkloadIdentityEnabled "true" }}
      appServiceAccountSecretName: "{{ $synchronizerName }}-{{ $generatedName }}-svc-account"
      {{- end }}
      nodeAffinityRequired: {{ $.NodeSelector.RequiredForScheduling }}
      {{- $affinityKey := (index $.NodeSelector.ApigeeRuntime "key") }}
      {{- $affinityValue := (index $.NodeSelector.ApigeeRuntime "value") }}
      {{- if (index $.Synchronizer.NodeSelector "key") }}
      {{- $affinityKey = (index $.Synchronizer.NodeSelector "key") }}
      {{- $affinityValue = (index $.Synchronizer.NodeSelector "value") }}
      {{- end}}
      {{- if $affinityKey }}
      nodeAffinity:
        {{- if $.NodeSelector.RequiredForScheduling }}
        requiredDuringSchedulingIgnoredDuringExecution:
          nodeSelectorTerms:
          - matchExpressions:
            - key: {{ $affinityKey }}
              operator: In
              values:
              - {{ $affinityValue }}
        {{- else }}
        preferredDuringSchedulingIgnoredDuringExecution:
        - weight: 100
          preference:
            matchExpressions:
            - key: {{ $affinityKey }}
              operator: In
              values:
              - {{ $affinityValue }}
        {{- end }}
      {{- end}}
      {{- $synchronizerVersion := $.Revision }}
      {{- if $.Synchronizer.Revision }}
      {{- $synchronizerVersion = $.Synchronizer.Revision }}
      {{- end }}
      version: "{{ validateVersion $synchronizerVersion }}"
      autoScaler:
        minReplicas: {{ $.Synchronizer.ReplicaCountMin }}
        maxReplicas: {{ $.Synchronizer.ReplicaCountMax }}
        metrics:
        - type: Resource
          resource:
            name: cpu
            targetAverageUtilization: {{ $.Synchronizer.TargetCPUUtilizationPercentage }}
      {{- if $env.PollInterval }}
      properties:
        pollInterval: {{ $env.PollInterval }}
      {{- end }}
      {{- if or $.HTTPProxy $.Synchronizer.CwcAppend }}
      configOverride:
      {{- if $.Synchronizer.CwcAppend }}
      {{- range $k, $v := $.Synchronizer.CwcAppend }}
        "{{ $k }}": "{{ $v }}"
      {{- end }}
      {{- end }}
      {{- if $.HTTPProxy }}
        "conf_http_client_service_jetty.proxy.enabled": "true"
        "conf_http_client_service_jetty.proxy.host": "{{ $.HTTPProxy.Host }}"
        {{- if $.HTTPProxy.Port }}
        "conf_http_client_service_jetty.proxy.port": "{{ $.HTTPProxy.Port }}"
        {{- end }}
        {{- if $.HTTPProxy.Username }}
        "conf_http_client_service_jetty.proxy.user": "{{ $.HTTPProxy.Username }}"
        {{- end }}
        {{- if $.HTTPProxy.Password }}
        "conf_http_client_service_jetty.proxy.password": "{{ base64EncodeString $.HTTPProxy.Password }}"
        {{- end }}
      {{- end }}
      {{- end }}
      initContainers:
      - name: {{ $schemaReadinessName }}
        image: "{{ $.MART.Image.URL }}:{{ $.MART.Image.Tag }}"
        imagePullPolicy: "{{ $.MART.Image.PullPolicy }}"
      containers:
      - name: {{ $synchronizerName }}
        image: "{{ $.Synchronizer.Image.URL }}:{{ $.Synchronizer.Image.Tag }}"
        imagePullPolicy: "{{ $.Synchronizer.Image.PullPolicy }}"
        livenessProbe:
          httpGet:
            path: /v1/probes/live
            scheme: HTTPS
            port: 8843
          initialDelaySeconds: {{ $.Synchronizer.LivenessProbe.InitialDelaySeconds }}
          periodSeconds: {{ $.Synchronizer.LivenessProbe.PeriodSeconds }}
          timeoutSeconds: {{ $.Synchronizer.LivenessProbe.TimeoutSeconds }}
          successThreshold: 1
          failureThreshold: {{ $.Synchronizer.LivenessProbe.FailureThreshold }}
        readinessProbe:
          httpGet:
            path: /v1/probes/ready
            scheme: HTTPS
            port: 8843
          initialDelaySeconds: {{ $.Synchronizer.ReadinessProbe.InitialDelaySeconds }}
          periodSeconds: {{ $.Synchronizer.ReadinessProbe.PeriodSeconds }}
          timeoutSeconds: {{ $.Synchronizer.ReadinessProbe.TimeoutSeconds }}
          successThreshold: {{ $.Synchronizer.ReadinessProbe.SuccessThreshold }}
          failureThreshold: {{ $.Synchronizer.ReadinessProbe.FailureThreshold }}
        resources:
          {{- if or $.Synchronizer.Resources.Limits.CPU $.Synchronizer.Resources.Limits.Memory }}
          limits:
            {{- if $.Synchronizer.Resources.Limits.CPU }}
            cpu: {{ $.Synchronizer.Resources.Limits.CPU }}
            {{- end }}
            {{- if $.Synchronizer.Resources.Limits.Memory }}
            memory: {{ $.Synchronizer.Resources.Limits.Memory }}
            {{- end }}
          {{- end }}
          requests:
            cpu: {{ $.Synchronizer.Resources.Requests.CPU }}
            memory: {{ $.Synchronizer.Resources.Requests.Memory }}
      - name: "{{ $authzName }}"
        securityContext:
          runAsNonRoot: true
          runAsUser: 100
          runAsGroup: 1000
          privileged: false
        image: "{{ $.Authz.Image.URL}}:{{ $.Authz.Image.Tag }}"
        imagePullPolicy: "{{ $.Authz.Image.PullPolicy }}"
        resources:
          requests:
            cpu: {{ $.Authz.Resources.Requests.CPU }}
            memory: {{ $.Authz.Resources.Requests.Memory }}
        readinessProbe:
          httpGet:
            path: /healthz
            port: 7091
          initialDelaySeconds: {{ $.Authz.ReadinessProbe.InitialDelaySeconds }}
          periodSeconds: {{ $.Authz.ReadinessProbe.PeriodSeconds }}
          timeoutSeconds: {{ $.Authz.ReadinessProbe.TimeoutSeconds }}
          successThreshold: {{ $.Authz.ReadinessProbe.SuccessThreshold }}
          failureThreshold: {{ $.Authz.ReadinessProbe.FailureThreshold }}
        livenessProbe:
          httpGet:
            path: /healthz
            port: 7091
          initialDelaySeconds: {{ $.Authz.LivenessProbe.InitialDelaySeconds }}
          periodSeconds: {{ $.Authz.LivenessProbe.PeriodSeconds }}
          timeoutSeconds: {{ $.Authz.LivenessProbe.TimeoutSeconds }}
          successThreshold: 1
          failureThreshold: {{ $.Authz.LivenessProbe.FailureThreshold }}
    udca:
      podServiceAccountName: {{ $udcaName }}-{{ $generatedName }}-sa
      {{- if ne $.GCP.WorkloadIdentityEnabled "true" }}
      appServiceAccountSecretName: "{{ $udcaName }}-{{ $generatedName }}-svc-account"
      {{- end }}
      nodeAffinityRequired: {{ $.NodeSelector.RequiredForScheduling }}
        {{- $affinityKey := (index $.NodeSelector.ApigeeRuntime "key") }}
        {{- $affinityValue := (index $.NodeSelector.ApigeeRuntime "value") }}
        {{- if (index $.UDCA.NodeSelector "key") }}
        {{- $affinityKey = (index $.UDCA.NodeSelector "key") }}
        {{- $affinityValue = (index $.UDCA.NodeSelector "value") }}
        {{- end}}
      {{- if $affinityKey }}
      nodeAffinity:
        {{- if $.NodeSelector.RequiredForScheduling }}
        requiredDuringSchedulingIgnoredDuringExecution:
          nodeSelectorTerms:
          - matchExpressions:
            - key: {{ $affinityKey }}
              operator: In
              values:
              - {{ $affinityValue }}
        {{- else }}
        preferredDuringSchedulingIgnoredDuringExecution:
        - weight: 100
          preference:
            matchExpressions:
            - key: {{ $affinityKey }}
              operator: In
              values:
              - {{ $affinityValue }}
        {{- end }}
      {{- end}}
      {{- $udcaVersion := $.Revision }}
      {{- if $.UDCA.Revision }}
      {{- $udcaVersion = $.UDCA.Revision }}
      {{- end }}
      version: "{{ validateVersion $udcaVersion }}"
      replicas: {{ $.UDCA.ReplicaCountMin }}
      autoScaler:
        minReplicas: {{ $.UDCA.ReplicaCountMin }}
        maxReplicas: {{ $.UDCA.ReplicaCountMax }}
        metrics:
        - type: Resource
          resource:
            name: cpu
            targetAverageUtilization: {{ $.UDCA.TargetCPUUtilizationPercentage }}
      initContainers:
      - name: {{ $udcaTlsReadinessName }}
        image: "{{ $.UDCA.Image.URL }}:{{ $.UDCA.Image.Tag }}"
        imagePullPolicy: "{{ $.UDCA.Image.PullPolicy }}"
      containers:
      - name: {{ $udcaName }}
        image: "{{ $.UDCA.Image.URL }}:{{ $.UDCA.Image.Tag }}"
        imagePullPolicy: "{{ $.UDCA.Image.PullPolicy }}"
        {{- if $.UDCA.EnvVars }}
        env:
        {{- range $k, $v := $.UDCA.EnvVars }}
        - name: "{{ $k }}"
          value: "{{ $v }}"
        {{- end }}
        {{- end }}
        livenessProbe:
          httpGet:
            path: /v1/probes/live
            port: 7070
            scheme: HTTPS
          initialDelaySeconds: {{ $.UDCA.LivenessProbe.InitialDelaySeconds }}
          periodSeconds: {{ $.UDCA.LivenessProbe.PeriodSeconds }}
          timeoutSeconds: {{ $.UDCA.LivenessProbe.TimeoutSeconds }}
          successThreshold: 1
          failureThreshold: {{ $.UDCA.LivenessProbe.FailureThreshold }}
        resources:
          {{- if or $.UDCA.Resources.Limits.CPU $.UDCA.Resources.Limits.Memory }}
          limits:
            {{- if $.UDCA.Resources.Limits.CPU }}
            cpu: {{ $.UDCA.Resources.Limits.CPU }}
            {{- end }}
            {{- if $.UDCA.Resources.Limits.Memory }}
            memory: {{ $.UDCA.Resources.Limits.Memory }}
            {{- end }}
          {{- end }}
          requests:
            cpu: {{ $.UDCA.Resources.Requests.CPU }}
            memory: {{ $.UDCA.Resources.Requests.Memory }}
      - name: "{{ $authzName }}"
        securityContext:
          runAsNonRoot: true
          runAsUser: 100
          runAsGroup: 1000
          privileged: false
        image: "{{ $.Authz.Image.URL}}:{{ $.Authz.Image.Tag }}"
        imagePullPolicy: "{{ $.Authz.Image.PullPolicy }}"
        resources:
          requests:
            cpu: {{ $.Authz.Resources.Requests.CPU }}
            memory: {{ $.Authz.Resources.Requests.Memory }}
        readinessProbe:
          httpGet:
            path: /healthz
            port: 7091
          initialDelaySeconds: {{ $.Authz.ReadinessProbe.InitialDelaySeconds }}
          periodSeconds: {{ $.Authz.ReadinessProbe.PeriodSeconds }}
          timeoutSeconds: {{ $.Authz.ReadinessProbe.TimeoutSeconds }}
          successThreshold: {{ $.Authz.ReadinessProbe.SuccessThreshold }}
          failureThreshold: {{ $.Authz.ReadinessProbe.FailureThreshold }}
        livenessProbe:
          httpGet:
            path: /healthz
            port: 7091
          initialDelaySeconds: {{ $.Authz.LivenessProbe.InitialDelaySeconds }}
          periodSeconds: {{ $.Authz.LivenessProbe.PeriodSeconds }}
          timeoutSeconds: {{ $.Authz.LivenessProbe.TimeoutSeconds }}
          successThreshold: 1
          failureThreshold: {{ $.Authz.LivenessProbe.FailureThreshold }}
      - name: "{{ $fluentdName }}"
        image: "{{ $.UDCA.Fluentd.Image.URL }}:{{ $.UDCA.Fluentd.Image.Tag }}"
        imagePullPolicy: {{ $.UDCA.Fluentd.Image.PullPolicy }}
        resources:
          requests:
            cpu: {{ $.UDCA.Fluentd.Resources.Requests.CPU }}
            memory: {{ $.UDCA.Fluentd.Resources.Requests.Memory }}
          limits:
            memory: {{ $.UDCA.Fluentd.Resources.Limits.Memory }}
    runtime:
      podServiceAccountName: {{ $runtimeName }}-{{ $generatedName }}-sa
      {{- if ne $.GCP.WorkloadIdentityEnabled "true" }}
      {{- if $env.ServiceAccountPaths.Runtime }}
      appServiceAccountSecretName: "{{ $runtimeName }}-{{ $generatedName }}-svc-account"
      {{- end }}
      {{- end }}
      nodeAffinityRequired: {{ $.NodeSelector.RequiredForScheduling }}
        {{- $affinityKey := (index $.NodeSelector.ApigeeRuntime "key") }}
        {{- $affinityValue := (index $.NodeSelector.ApigeeRuntime "value") }}
        {{- if (index $.Runtime.NodeSelector "key") }}
        {{- $affinityKey = (index $.Runtime.NodeSelector "key") }}
        {{- $affinityValue = (index $.Runtime.NodeSelector "value") }}
        {{- end}}
      {{- if $affinityKey }}
      nodeAffinity:
        {{- if $.NodeSelector.RequiredForScheduling }}
        requiredDuringSchedulingIgnoredDuringExecution:
          nodeSelectorTerms:
          - matchExpressions:
            - key: {{ $affinityKey }}
              operator: In
              values:
              - {{ $affinityValue }}
        {{- else }}
        preferredDuringSchedulingIgnoredDuringExecution:
        - weight: 100
          preference:
            matchExpressions:
            - key: {{ $affinityKey }}
              operator: In
              values:
              - {{ $affinityValue }}
        {{- end }}
      {{- end}}
      {{- $runtimeVersion := $.Revision }}
      {{- if $.Runtime.Revision }}
      {{- $runtimeVersion = $.Runtime.Revision }}
      {{- end }}
      version: "{{ validateVersion $runtimeVersion }}"
      replicas: {{ $.Runtime.ReplicaCountMin }}
      autoScaler:
        minReplicas: {{ $.Runtime.ReplicaCountMin }}
        maxReplicas: {{ $.Runtime.ReplicaCountMax }}
        metrics:
        - type: Resource
          resource:
            name: cpu
            targetAverageUtilization: {{ $.Runtime.TargetCPUUtilizationPercentage }}
      {{- if or ($.Runtime.CwcAppend) ($env.HTTPProxy)}}
      configOverride:
      {{- range $k, $v := $.Runtime.CwcAppend }}
        "{{ $k }}": "{{ $v }}"
      {{- end }}
      {{- if $env.HTTPProxy }}
        "conf_http_HTTPClient.use.proxy": "true"
        "conf/http.properties+HTTPClient.proxy.host": "{{ $env.HTTPProxy.Host }}"
        {{- if $env.HTTPProxy.Scheme }}
        "conf/http.properties+HTTPClient.proxy.type": "{{ toUpper $env.HTTPProxy.Scheme }}"
        {{- else }}
        "conf/http.properties+HTTPClient.proxy.type": "HTTP"
        {{- end }}
        {{- if $env.HTTPProxy.Port }}
        "conf/http.properties+HTTPClient.proxy.port": "{{ $env.HTTPProxy.Port }}"
        {{- end }}
        {{- if and $env.HTTPProxy.Username $env.HTTPProxy.Password }}
        "conf/http.properties+HTTPClient.proxy.user": {{ $env.HTTPProxy.Username }}
        "conf/http.properties+HTTPClient.proxy.password": {{ $env.HTTPProxy.Password }}
        {{- end }}
      {{- end }}
      {{- end }}
      initContainers:
      - name: {{ $schemaReadinessName }}
        image: "{{ $.MART.Image.URL }}:{{ $.MART.Image.Tag }}"
        imagePullPolicy: "{{ $.MART.Image.PullPolicy }}"
      containers:
      - name: {{ $runtimeName }}
        image: "{{ $.Runtime.Image.URL }}:{{ $.Runtime.Image.Tag }}"
        imagePullPolicy: "{{ $.Runtime.Image.PullPolicy }}"
        env:
        - name: "{{ hyphenToUnderscoreUpper $.Org }}_KMS_ENCRYPTION_KEY"
          valueFrom:
            secretKeyRef:
              {{- if $.KMSEncryptionSecret }}
              name: {{ $.KMSEncryptionSecret.Name }}
              key: {{ $.KMSEncryptionSecret.Key }}
              {{- else }}
              name: "{{ $generatedName }}-encryption-keys"
              key: kmsEncryptionKey
              {{- end }}
        - name: "{{ hyphenToUnderscoreUpper $.Org }}_{{ hyphenToUnderscoreUpper $env.Name }}_CACHE_ENCRYPTION_KEY"
          valueFrom:
            secretKeyRef:
              {{- if $env.CacheEncryptionSecret }}
              name:  {{ $env.CacheEncryptionSecret.Name }}
              key: {{ $env.CacheEncryptionSecret.Key }}
              {{- else }}
              name: "{{ $generatedName }}-encryption-keys"
              key: cacheEncryptionKey
              {{- end }}
        - name: "{{ hyphenToUnderscoreUpper $.Org }}_{{ hyphenToUnderscoreUpper $env.Name }}_KVM_ENCRYPTION_KEY"
          valueFrom:
            secretKeyRef:
              {{- if $env.KVMEncryptionSecret }}
              name: {{ $env.KVMEncryptionSecret.Name }}
              key: {{ $env.KVMEncryptionSecret.Key }}
              {{- else }}
              name: "{{ $generatedName }}-encryption-keys"
              key: envKvmEncryptionKey
              {{- end }}
        - name: "{{ hyphenToUnderscoreUpper $.Org }}_KVM_ENCRYPTION_KEY"
          valueFrom:
            secretKeyRef:
              {{- if $.KVMEncryptionSecret }}
              name: {{ $.KVMEncryptionSecret.Name }}
              key: {{ $.KVMEncryptionSecret.Key }}
              {{- else }}
              name: "{{ $generatedName }}-encryption-keys"
              key: kvmEncryptionKey
              {{- end }}
        livenessProbe:
          httpGet:
            path: "/v1/probes/live"
            scheme: HTTPS
            port: 8843
          initialDelaySeconds: {{ $.Runtime.LivenessProbe.InitialDelaySeconds }}
          periodSeconds: {{ $.Runtime.LivenessProbe.PeriodSeconds }}
          timeoutSeconds: {{ $.Runtime.LivenessProbe.TimeoutSeconds }}
          successThreshold: 1
          failureThreshold: {{ $.Runtime.LivenessProbe.FailureThreshold }}
        readinessProbe:
          httpGet:
            path: /v1/probes/ready
            scheme: HTTPS
            port: 8843
          initialDelaySeconds: {{ $.Runtime.ReadinessProbe.InitialDelaySeconds }}
          periodSeconds: {{ $.Runtime.ReadinessProbe.PeriodSeconds }}
          timeoutSeconds: {{ $.Runtime.ReadinessProbe.TimeoutSeconds }}
          successThreshold: {{ $.Runtime.ReadinessProbe.SuccessThreshold }}
          failureThreshold: {{ $.Runtime.ReadinessProbe.FailureThreshold }}
        resources:
          requests:
            cpu: {{ $.Runtime.Resources.Requests.CPU }}
            memory: {{ $.Runtime.Resources.Requests.Memory }}
          {{- if or $.Runtime.Resources.Limits.CPU $.Runtime.Resources.Limits.Memory }}
          limits:
            {{- if $.Runtime.Resources.Limits.CPU }}
            cpu: {{ $.Runtime.Resources.Limits.CPU }}
            {{- end }}
            {{- if $.Runtime.Resources.Limits.Memory }}
            memory: {{ $.Runtime.Resources.Limits.Memory }}
            {{- end }}
          {{- end }}
        volumeMounts:
        - name: java-sec-policy-volume
          readOnly: true
          mountPath: {{ $.RuntimeInstallDir }}/security
        - name: policy-secret-volume
          readOnly: true
          mountPath: {{ $.RuntimeInstallDir }}/policy-secrets
      volumes:
      - name: java-sec-policy-volume
        secret:
          secretName: "{{ $.Org }}-{{ $env.Name }}-java-sec-policy"
          optional: true
      - name: policy-secret-volume
        secret:
          secretName: "{{ $.Org }}-{{ $env.Name }}-policy-secret"
          optional: true
{{- end -}}
{{- end -}}
{{- end -}}