#!/bin/bash
function message() {
  msg=$1
  echo "****************************"
  echo "$1"
  echo "Press any key to continue..."
  echo "****************************"
  read -n 1
}
terraform init
terraform version
echo "Please first prepare a github for ACM and a Service account key"
echo "Modify the corresponding variable.tf"

message "Create GKE cluster"
terraform apply -auto-approve -target module.gke

message "Register GKE to Anthos Hub"
terraform apply -auto-approve -target module.hub-primary

message "Install ASM 1.8"
terraform apply -auto-approve -target null_resource.install-asm

message "Sign Cert"
terraform apply -auto-approve -target module.cert-manager
terraform apply -auto-approve -target null_resource.signed_cert
message "Install Apigee"
terraform apply -auto-approve -target null_resource.install-apigee
