#!/bin/bash
function message() {
  msg=$1
  echo "****************************"
  echo "$1"
  echo "Press any key to continue..."
  echo "****************************"
  read -n 1
}
echo "Remove Apigee Installation"

message "Remove Apigee"
terraform destroy -auto-approve -target null_resource.install-apigee

message "Remove Cert"
terraform destroy -auto-approve -target null_resource.signed_cert
terraform destroy -auto-approve -target module.cert-manager

message "Remove ASM 1.8"
terraform destroy -auto-approve -target module.asm-primary

message "Un-register GKE to Anthos Hub"
terraform destroy -auto-approve -target module.hub-primary

message "Remove GKE cluster"
terraform destroy -auto-approve -target module.gke

message "Disable Anthos Services"
terraform destroy -auto-approve -target module.project-services
