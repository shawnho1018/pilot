if [[ "${OSTYPE}" == "linux-gnu"* ]]; then
  echo "download OS version"
  gsutil cp gs://apigee-release/hybrid/apigee-hybrid-setup/${VERSION}/apigeectl_linux_64.tar.gz ./
  tar zxvf apigeectl_linux_64.tar.gz -C ./
  mv apigeectl_${VERSION}-d591b23_linux_64 apigeectl
  rm -f apigeectl_linux_64.tar.gz
elif [[ "${OSTYPE}" == "darwin"* ]]; then
  echo "download mac version"
  gsutil cp gs://apigee-release/hybrid/apigee-hybrid-setup/${VERSION}/apigeectl_mac_64.tar.gz ./
  curl -LO https://storage.googleapis.com/apigee-release/hybrid/apigee-hybrid-setup/${VERSION}/apigeectl_mac_64.tar.gz
  tar zxvf apigeectl_mac_64.tar.gz -C ./
  mv apigeectl_${VERSION}-d591b23_mac_64 apigeectl
  rm -f apigeectl_mac_64.tar.gz
fi
APIGEECTL_HOME=$(pwd)/apigeectl
mkdir -p hybrid-files
mkdir -p hybrid-files/overrides
mkdir -p hybrid-files/service-accounts
mkdir -p hybrid-files/certs
ln -s $APIGEECTL_HOME/tools hybrid-files/tools
ln -s $APIGEECTL_HOME/config hybrid-files/config
cp -r $APIGEECTL_HOME/templates hybrid-files/templates
ln -s $APIGEECTL_HOME/plugins hybrid-files/plugins

for id in org-admin synchronizer udca cassandra logger mart metrics watcher distributed-trace; do
    echo "Produce-${id}-key"
    echo 'y' | ./hybrid-files/tools/create-service-account apigee-${id} ./hybrid-files/service-accounts/
done
gcloud projects add-iam-policy-binding ${APIGEE_PROJECT} \
--member serviceAccount:apigee-org-admin@${APIGEE_PROJECT}.iam.gserviceaccount.com \
--role roles/apigee.admin
