#!/bin/bash
sudo certbot certonly --dns-google -d *.shawnk8s.com,*.shawnk8s.com --server https://acme-v02.api.letsencrypt.org/directory --dns-google-credentials ~/workspace/gcp-keys/anthos-demo-280104-clouddns.key
sudo cp -r /etc/letsencrypt/archive/shawnk8s.com ./hybrid-files/
sudo chown -R shawnho:shawnho ./hybrid-files/shawnk8s.com
kubectl create -n istio-system secret generic cert-default  \
--from-file=key=hybrid-files/shawnk8s.com/privkey1.pem \
--from-file=cert=hybrid-files/shawnk8s.com/fullchain1.pem -n istio-system
