module "gke" {
  source                     = "terraform-google-modules/kubernetes-engine/google//modules/private-cluster"
  project_id                 = var.project_id
  name                       = var.cluster_name
  region                     = var.primary_region
  zones                      = var.primary_zones
  network                    = var.vpc
  subnetwork                 = var.subnet
  ip_range_pods              = var.secondary-iprange-pods
  ip_range_services          = var.secondary-iprange-services
  http_load_balancing        = true
  horizontal_pod_autoscaling = true
  network_policy             = true
  enable_private_nodes       = true

  node_pools = [
    {
      name               = "default-node-pool"
      machine_type       = "n1-standard-4"
      min_count          = 1
      max_count          = 5
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      service_account    = "anthos-install@wlouis-vpc-sc-lab.iam.gserviceaccount.com"
      preemptible        = false
      initial_node_count = 2
    },
  ]

  node_pools_labels = {
    all = {}

    default-node-pool = {
      default-node-pool = "true"
    }
  }

  node_pools_metadata = {
    all = {}

    default-node-pool = {
      node-pool-metadata-custom-value = "my-node-pool"
    }
  }

  node_pools_taints = {
    all = []

    default-node-pool = [
      {
        key    = "default-node-pool"
        value  = "true"
        effect = "PREFER_NO_SCHEDULE"
      },
    ]
  }

  node_pools_tags = {
    all = []

    default-node-pool = [
      "default-node-pool",
    ]
  }
}