module "apigee-services" {
  source  = "terraform-google-modules/project-factory/google//modules/project_services"
  project_id  = var.project_id
  disable_services_on_destroy = false
  activate_apis = [
    "anthos.googleapis.com",
    "apigee.googleapis.com",
    "apigeeconnect.googleapis.com",
    "pubsub.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "compute.googleapis.com",
    "container.googleapis.com",
  ]
}

module "cert-manager" {
  source = "terraform-google-modules/gcloud/google//modules/kubectl-wrapper"
  project_id              = var.project_id
  cluster_name            = var.cluster_name
  cluster_location        = var.primary_zones[0]
  enabled                 = true
  kubectl_create_command  = "kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v1.2.0/cert-manager.yaml"
  kubectl_destroy_command = "kubectl delete -f https://github.com/jetstack/cert-manager/releases/download/v1.2.0/cert-manager.yaml"
  module_depends_on       = [module.apigee-services]
}

resource "null_resource" "signed_cert" {
  provisioner "local-exec" {
    command = "${path.module}/scripts/gen-cert.sh"
    interpreter = ["/bin/zsh"]
    environment = {
        ACCOUNT_EMAIL    = var.cert_user_email
        CLOUDDNS_PROJECT = var.dns_project_id
        DOMAIN_NAME      = var.domain_name
        CERT_KEY_PATH    = var.gcp_key_clouddns_path
        CERT_KEY         = var.gcp_key_clouddns_name
    }
  }
  provisioner "local-exec" {
    when    = destroy
    command = <<-EOT
      echo "run destroy command......................................................"
      kubectl delete secret cert-admin -n istio-system
      kubectl delete -f ./cert-issuer.yaml
    EOT
  }
}
resource "null_resource" "download-apigee" {
  provisioner "local-exec" {
      command = "${path.module}/scripts/download-apigee.sh"
      interpreter = ["/bin/zsh"]
      environment = {
        VERSION        = var.apigee_version
        APIGEE_PROJECT = var.project_id
    }  
  }
  provisioner "local-exec" {
    when   = destroy
    command = <<-EOT
      echo "Skip hybrid-files folder deletion"
      #rm -rf hybrid-files/
    EOT
  }
}

resource "null_resource" "install-apigee" {
  provisioner "local-exec" {
      command = "${path.module}/scripts/install-apigee.sh"
      interpreter = ["/bin/zsh"]
      environment = {
        APIGEE_REGION  = var.apigee_region
        APIGEE_PROJECT = var.project_id
        REGION         = var.primary_region
        CLUSTER_NAME   = var.cluster_name
    }
  }
  provisioner "local-exec" {
    when   = destroy
    command = <<-EOT
      echo "Delete apigee installation"
      #kubectl delete -f hybrid-files/overrides/overrides-apigee.yaml
      kubectl delete ns apigee
      kubectl delete ns apigee-system
    EOT
  }  
}