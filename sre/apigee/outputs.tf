output "project" {
  value = data.google_client_config.current.project
}
output "project_number" {
  value = data.google_project.project.number
}
output "istio_external_ip" {
  value = module.istio.external-ip
}

