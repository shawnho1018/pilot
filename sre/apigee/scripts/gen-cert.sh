#!/bin/zsh
kubectl create secret generic cert-admin -n istio-system --from-file=${CERT_KEY_PATH}

cat <<- EOT > ./cert-issuer.yaml
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: clouddns-issuer
  namespace: istio-system
spec:
  acme:
    email: ${ACCOUNT_EMAIL}
    server: https://acme-staging-v02.api.letsencrypt.org/directory
    privateKeySecretRef:
      name: clouddns-issuer
    solvers:
    - dns01:
        cloudDNS:
          # The ID of the GCP project
          project: ${CLOUDDNS_PROJECT}
          # This is the secret used to access the service account
          serviceAccountSecretRef:
            name: cert-admin
            key: ${CERT_KEY}
---
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: cert-default
  namespace: istio-system
spec:
  secretName: cert-default
  issuerRef:
    name: clouddns-issuer
  commonName: "*.${DOMAIN_NAME}"
  dnsNames:
  - "*.${DOMAIN_NAME}"
EOT
kubectl apply -f ./cert-issuer.yaml
kubectl wait certificate/cert-default -n istio-system --for condition=ready --timeout=300s