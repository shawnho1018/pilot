#!/bin/zsh
function enable_apigee() {
    echo "enable apigee config..."
    export GOOGLE_APPLICATION_CREDENTIALS=./service-accounts/${APIGEE_PROJECT}-apigee-org-admin.json
    export TOKEN=$(gcloud auth application-default print-access-token --project ${APIGEE_PROJECT})
    curl -X POST -H "Authorization: Bearer $TOKEN" \
        -H "Content-Type:application/json" \
        "https://apigee.googleapis.com/v1/organizations/${APIGEE_PROJECT}:setSyncAuthorization" \
        -d '{"identities":["'"serviceAccount:apigee-synchronizer@${APIGEE_PROJECT}.iam.gserviceaccount.com"'"]}'
    curl -X POST -H "Authorization: Bearer $TOKEN" \
        -H "Content-Type:application/json" \
        "https://apigee.googleapis.com/v1/organizations/${APIGEE_PROJECT}:getSyncAuthorization" \
        -d ''

    echo "Enable apigee connect..."
    curl -H "Authorization: Bearer $TOKEN" \
        "https://apigee.googleapis.com/v1/organizations/${APIGEE_PROJECT}"
    curl -H "Authorization: Bearer $TOKEN" -X PUT \
        -H "Content-Type: application/json" \
        -d '{
            "name" : "'"${APIGEE_PROJECT}"'",
            "properties" : {
                "property" : [ {
                    "name" : "features.hybrid.enabled",
                    "value" : "true"
            }, {
                "name" : "features.mart.connect.enabled",
                "value" : "true"
                } ]
            }
        }' \
        "https://apigee.googleapis.com/v1/organizations/${APIGEE_PROJECT}"
}
cd hybrid-files
cat <<- EOT > ./overrides/overrides-apigee.yaml
# GCP project name where the org is provisioned.
gcp:
  region: ${APIGEE_REGION}
  projectID: ${APIGEE_PROJECT}

# Apigee org name.
org: ${APIGEE_PROJECT}

# Kubernetes cluster name details
k8sCluster:
  name: ${CLUSTER_NAME}
  region: ${REGION}

# unique identifier for this installation.
instanceID: "${CLUSTER_NAME}-${APIGEE_PROJECT}-1"

virtualhosts:
  - name: apigee-dev
    sslSecret: cert-default

envs:
    # Apigee environment name.
  - name: dev
    # Service accounts for sync and UDCA.
    serviceAccountPaths:
      synchronizer: ./service-accounts/${APIGEE_PROJECT}-apigee-synchronizer.json
      udca: ./service-accounts/${APIGEE_PROJECT}-apigee-udca.json
mart:
  serviceAccountPath: ./service-accounts/${APIGEE_PROJECT}-apigee-mart.json
metrics:
  serviceAccountPath: ./service-accounts/${APIGEE_PROJECT}-apigee-metrics.json
connectAgent:
  serviceAccountPath: ./service-accounts/${APIGEE_PROJECT}-apigee-mart.json
watcher:
  serviceAccountPath: ./service-accounts/${APIGEE_PROJECT}-apigee-watcher.json
logger:
  enabled: false
  serviceAccountPath: ./service-accounts/${APIGEE_PROJECT}-apigee-logger.json
EOT
# install apigee operator
echo "##################### apigeectl init #########################"
apigeectl init -f ./overrides/overrides-apigee.yaml
kubectl wait -n apigee-system pods -l control-plane=controller-manager --for condition=ready --timeout 300s
kubectl wait -n apigee-system jobs -l job-name=apigee-resources-install --for condition=complete --timeout 300s

echo "##################### apigeectl check-ready ##################"
apigeectl check-ready -f ./overrides/overrides-apigee.yaml
echo "##################### apigeectl apply ########################"
apigeectl apply -f ./overrides/overrides-apigee.yaml
kubectl wait pods -l app=apigee-runtime --for condition=ready -n apigee --timeout 600s
enable_apigee