variable "project_id" {
  description = "The project ID to host the cluster in"
  default = "wlouis-vpc-sc-lab"
}
variable "cluster_name" {
  description = "Name of the cluster"
  default = "cluster-apigee-1"
}
variable "primary_region" {
  description = "The primary region to be used"
  default = "asia-east1"
}
variable "primary_zones" {
  description = "The primary zones to be used"
  default = ["asia-east1-a"]
}

variable "vpc" {
  description = "private vpc name"
  default     = "wlouis-onprem-vpc"
}

variable "subnet" {
  description = "private subnet name"
  default     = "onprem-subnet-1"
}

variable "secondary-iprange-pods" {
  description = "secondary iprange for pods"
  default     = "asia-east1-vpc-pods"
}
variable "secondary-iprange-services" {
  description = "secondary iprange for services"
  default     = "asia-east1-vpc-services"
}

variable "gcp_key_path" {
  description = "Existing GCP service account key path"
  default = "/Users/shawnho/workspace/gcp-keys/wlouis-vpc-sc-lab-sa.key"
}

variable "acm_repo_location" {
  description = "The location of the git repo ACM will sync to"
  default = "ssh://shawnho@google.com@source.developers.google.com:2022/p/shawn-demo-2021/r/config-repo"
}
variable "acm_branch" {
  description = "The git branch ACM will sync to"
  default = "master"
}
variable "ssh_auth_key_path" {
  description = "SSH-key path for git repo access"
  default = "keys/repo.key"
}

variable "gcp_key_clouddns_path" {
  description = "Service account key path which holds CloudDNS access permission"
  default = "/Users/shawnho/workspace/gcp-keys/anthos-demo-280104-clouddns.key"
}
variable "dns_project_id" {
  description = "The project ID to access CloudDNS"
  default = "anthos-demo-280104"
}
variable "cert_user_email" {
  description = "email for cert"
  default = "shawnho@google.com"
}

variable "domain_name" {
  description = "Domain name"
  default     = "shawnk8s.com"
}

variable "gcp_key_clouddns_name" {
  description = "Key name used for Project"
  default     = "anthos-demo-280104-clouddns.key"
}
variable "apigee_version" {
  description = "Apigee installed version"
  default     = "1.4.0"
}
variable "apigee_region" {
  description = "Apigee installed zone"
  default     = "asia-east1"
}
