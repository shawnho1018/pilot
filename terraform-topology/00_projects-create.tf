data "google_billing_account" "acct" {
  billing_account = "billingAccounts/017D2E-F0158F-AF57ED"
  open         = true
}

resource "google_project" "spoke" {
  name       = var.spoke_project_id
  project_id = var.spoke_project_id
  folder_id     = "623112070785"
  billing_account = data.google_billing_account.acct.id
  skip_delete = true
}

resource "google_project" "hub" {
  name       = var.hub_project_id
  project_id = var.hub_project_id
  folder_id     = "623112070785"
  billing_account = data.google_billing_account.acct.id
  skip_delete = true
}