module "hub-services" {
  source  = "terraform-google-modules/project-factory/google//modules/project_services"
  project_id  = google_project.hub.project_id
  disable_services_on_destroy = false
  activate_apis = [
    "anthos.googleapis.com",
    "compute.googleapis.com",
    "container.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "clouderrorreporting.googleapis.com",
    "cloudtrace.googleapis.com",    
    "dns.googleapis.com",
    "gkehub.googleapis.com",
    "gkeconnect.googleapis.com", 
    "iam.googleapis.com",
    "iamcredentials.googleapis.com",
    "logging.googleapis.com",
    "meshca.googleapis.com",
    "meshtelemetry.googleapis.com",
    "meshconfig.googleapis.com",
    "monitoring.googleapis.com"
  ]
}
module "spoke-services" {
  source  = "terraform-google-modules/project-factory/google//modules/project_services"
  project_id  = google_project.spoke.project_id
  disable_services_on_destroy = false
  activate_apis = [
    "compute.googleapis.com",
    "container.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "clouderrorreporting.googleapis.com",
    "cloudtrace.googleapis.com",    
    "dns.googleapis.com",
    "iam.googleapis.com",
    "iamcredentials.googleapis.com",
    "logging.googleapis.com",
    "meshca.googleapis.com",
    "meshtelemetry.googleapis.com",
    "meshconfig.googleapis.com",
    "monitoring.googleapis.com"
  ]
}
