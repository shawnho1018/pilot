resource "google_compute_network" "spoke_vpc" {
  name    = "vpc-network"
  auto_create_subnetworks = false
  project = google_project.spoke.project_id
}

resource "google_compute_subnetwork" "spoke_vpc_dmz" {
  name          = "dmz-subnet"
  ip_cidr_range = "10.2.0.0/16"
  region        = var.region
  network       = google_compute_network.spoke_vpc.id
  project = google_project.spoke.project_id
  secondary_ip_range {
    range_name    = "dmz-service-ipam"
    ip_cidr_range = "10.100.0.0/24"
  }
  secondary_ip_range {
    range_name    = "dmz-pod-ipam"
    ip_cidr_range = "10.100.16.0/20"
  }  
}

resource "google_compute_network" "hub_vpc" {
  name    = "vpc-network"
  auto_create_subnetworks = false
  project = google_project.hub.project_id
}

resource "google_compute_subnetwork" "hub_vpc_intranet" {
  name          = "intranet-subnet"
  ip_cidr_range = "10.3.0.0/16"
  region        = var.region
  project = google_project.hub.project_id
  network       = google_compute_network.hub_vpc.id
  secondary_ip_range {
    range_name    = "intranet-service-ipam"
    ip_cidr_range = "10.101.0.0/24"
  }
  secondary_ip_range {
    range_name    = "intranet-pod-ipam"
    ip_cidr_range = "10.101.16.0/20"
  }
  secondary_ip_range {
    range_name    = "intranet-service-ipam-2"
    ip_cidr_range = "10.101.1.0/24"
  }
  secondary_ip_range {
    range_name    = "intranet-pod-ipam-2"
    ip_cidr_range = "10.101.32.0/20"
  }  
}

# proxy-only subnet
resource "google_compute_subnetwork" "intranet-proxy-subnet" {
  name          = "l7-ilb-intranet-proxy-subnet"
  provider      = google-beta
  ip_cidr_range = "10.5.0.0/24"
  region        = var.region
  purpose       = "INTERNAL_HTTPS_LOAD_BALANCER"
  role          = "ACTIVE"
  network       = google_compute_network.hub_vpc.id
  project       = google_project.hub.project_id
}

# vpc-peering
resource "google_compute_network_peering" "spoke2hub" {
  name         = "peering1"
  network      = google_compute_network.spoke_vpc.id
  peer_network = google_compute_network.hub_vpc.id
}
resource "google_compute_network_peering" "hub2spoke" {
  name         = "peering1"
  network      = google_compute_network.hub_vpc.id
  peer_network = google_compute_network.spoke_vpc.id
}

# cloud-router
resource "google_compute_router" "intranet-router" {
  name    = "intranet-router"
  region  = var.region
  network = google_compute_network.hub_vpc.id
  project = google_project.hub.project_id
}
# cloud nat
resource "google_compute_router_nat" "intranet-nat" {
  name                               = "intranet-nat"
  router                             = google_compute_router.intranet-router.name
  region                             = google_compute_router.intranet-router.region
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
  project                            = google_project.hub.project_id
  log_config {
    enable = true
    filter = "ERRORS_ONLY"
  }
}