resource "google_dns_managed_zone" "intranet-googleapis" {
  name     = "googleapis"
  dns_name = "googleapis.com."
  visibility = "private"
  project = google_project.hub.project_id
  private_visibility_config {
    networks {
      #network_url = google_compute_network.cluster-hybrid-network.id
      network_url = google_compute_network.hub_vpc.id
    }
  }
}

resource "google_dns_record_set" "intranet-googleapis-a" {
  name         = "private.googleapis.com."
  managed_zone = google_dns_managed_zone.intranet-googleapis.name
  type         = "A"
  ttl          = 60
  project = google_project.hub.project_id
  rrdatas = var.google_private_ips
}

resource "google_dns_record_set" "intranet-googleapis-cname" {
  name         = "*.googleapis.com."
  managed_zone = google_dns_managed_zone.intranet-googleapis.name
  type         = "CNAME"
  ttl          = 60
  project = google_project.hub.project_id
  rrdatas = ["private.googleapis.com."]
}

resource "google_dns_managed_zone" "intranet-gcr" {
  name     = "gcr"
  dns_name = "gcr.io."
  visibility = "private"
  project = google_project.hub.project_id
  private_visibility_config {
    networks {
      network_url = google_compute_network.hub_vpc.id
    }
  }
}

resource "google_dns_record_set" "intranet-gcr-a" {
  name         = "gcr.io."
  managed_zone = google_dns_managed_zone.intranet-gcr.name
  type         = "A"
  ttl          = 6
  project = google_project.hub.project_id
  rrdatas = var.google_private_ips
}

resource "google_dns_record_set" "intranet-gcr-cname" {
  name         = "*.gcr.io."
  managed_zone = google_dns_managed_zone.intranet-gcr.name
  type         = "CNAME"
  ttl          = 60
  project = google_project.hub.project_id
  rrdatas = ["gcr.io."]
}

resource "google_dns_managed_zone" "intranet-pkg" {
  name     = "pkg"
  dns_name = "pkg.dev."
  visibility = "private"
  project = google_project.hub.project_id
  private_visibility_config {
    networks {
      network_url = google_compute_network.hub_vpc.id
    }
  }
}

resource "google_dns_record_set" "intranet-pkg-a" {
  name         = "pkg.dev."
  managed_zone = google_dns_managed_zone.intranet-pkg.name
  type         = "A"
  ttl          = 60
  project = google_project.hub.project_id
  rrdatas = var.google_private_ips
}

resource "google_dns_record_set" "intranet-pkg-cname" {
  name         = "*.pkg.dev."
  managed_zone = google_dns_managed_zone.intranet-pkg.name
  type         = "CNAME"
  ttl          = 60
  project = google_project.hub.project_id
  rrdatas = ["pkg.dev."]
}