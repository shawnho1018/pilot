resource "google_compute_firewall" "allow-master-webhook" {
 name    = "gke-master-cluster-webhooks"
 network = google_compute_network.hub_vpc.name
 project = google_project.hub.project_id
 direction = "INGRESS"

 allow {
   protocol = "tcp"
   ports    = ["9443"]
 }

 target_tags = ["cluster-runtime"]
 source_ranges = [
     "10.4.0.0/27",
     "10.3.0.0/16",
     "10.101.0.0/24",
     "10.101.16.0/20"
     ]
}

resource "google_compute_firewall" "allow-healthcheck" {
 name    = "gke-master-healthcheck"
 network = google_compute_network.hub_vpc.name
 project = google_project.hub.project_id
 direction = "INGRESS"

 allow {
   protocol = "tcp"
 }

 target_tags = ["pod-healthcheck"]
 source_ranges = [
    "35.191.0.0/16",
    "130.211.0.0/22",
    "35.235.240.0/20"  
  ]
}

resource "google_compute_firewall" "allow-google-private-egress" {
  name    = "allow-google-private-egress"
  network = google_compute_network.hub_vpc.name
  project = google_project.hub.project_id
  direction = "EGRESS"
  priority = 1000

  allow {
     protocol = "tcp"
     ports = ["443"]
  }
  destination_ranges = ["199.36.153.8/30"]
}

resource "google_compute_firewall" "allow-internal-egress" {
  name    = "allow-internal-egress"
  network = google_compute_network.hub_vpc.name
  project = google_project.hub.project_id
  direction = "EGRESS"
  priority = 1000

  allow {
     protocol = "all"
  }
  destination_ranges = [
     "10.4.0.0/27",
     "10.3.0.0/16",
     "10.101.0.0/24",
     "10.101.16.0/20"
  ]
}
