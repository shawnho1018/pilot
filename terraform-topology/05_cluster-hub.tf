data "google_service_account" "sa-cluster" {
  account_id   = var.cluster_service_account_id
  project      = google_project.hub.project_id
}

resource "google_container_cluster" "cluster-1" {

  provider = google-beta
  name               = "pcluster-1"
  location           = var.zone
  node_locations     = null

  release_channel {
    channel = "REGULAR"
  }

  # see node pools
  initial_node_count = "1"
  remove_default_node_pool = "true"
  # datapath_provider
  datapath_provider = "ADVANCED_DATAPATH"
  # network
  networking_mode = "VPC_NATIVE"
  network            = google_compute_network.hub_vpc.name
  subnetwork         = google_compute_subnetwork.hub_vpc_intranet.name
  project            = google_project.hub.project_id
  master_authorized_networks_config {
    cidr_blocks {
        cidr_block   = "0.0.0.0/0"
        display_name = "allow all IPs to access the master"
    }
  }

  private_cluster_config {
    enable_private_nodes = "true"
    enable_private_endpoint = "false"
    master_ipv4_cidr_block = "10.4.0.0/28"
  }

  ip_allocation_policy {
    # pod subnet range
    cluster_secondary_range_name  = "intranet-pod-ipam"
    # services subnet range
    services_secondary_range_name = "intranet-service-ipam"
  }

  resource_labels = {
    mesh_id="proj-${google_project.hub.number}"
  }

  workload_identity_config {
    identity_namespace = "${google_project.hub.project_id}.svc.id.goog"
  }

  authenticator_groups_config {
    security_group = "gke-security-groups@${var.security_group_domain}"
  }

  logging_service="logging.googleapis.com/kubernetes"
  monitoring_service="monitoring.googleapis.com/kubernetes"

  addons_config {

    dns_cache_config {
      enabled = true
    }
  }
}

resource "google_container_node_pool" "nodepool-1" {
  name       = "nodepool-1"
  location           = var.zone
  node_locations     = [var.zone]

  cluster    = google_container_cluster.cluster-1.name
  project    = google_project.hub.project_id
  initial_node_count = 2

  autoscaling {
    min_node_count = 2
    max_node_count = 4
  }

  node_config {
    machine_type = var.vm_type_runtime
    service_account = data.google_service_account.sa-cluster.email

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
        "https://www.googleapis.com/auth/cloud-platform"
    ]

    tags = [google_container_cluster.cluster-1.name, "cluster-runtime","cluster-${google_project.hub.project_id}"]
  }
}


resource "google_container_cluster" "cluster-2" {

  provider = google-beta
  name               = "pcluster-2"
  location           = var.zone
  node_locations     = null

  release_channel {
    channel = "REGULAR"
  }

  # see node pools
  initial_node_count = "1"
  remove_default_node_pool = "true"
  # datapath_provider
  datapath_provider = "ADVANCED_DATAPATH"
  # network
  networking_mode = "VPC_NATIVE"
  network            = google_compute_network.hub_vpc.name
  subnetwork         = google_compute_subnetwork.hub_vpc_intranet.name
  project            = google_project.hub.project_id
  master_authorized_networks_config {
    cidr_blocks {
        cidr_block   = "0.0.0.0/0"
        display_name = "allow all IPs to access the master"
    }
  }

  private_cluster_config {
    enable_private_nodes = "true"
    enable_private_endpoint = "false"
    master_ipv4_cidr_block = "10.4.0.16/28"
  }

  ip_allocation_policy {
    # pod subnet range
    cluster_secondary_range_name  = "intranet-pod-ipam-2"
    # services subnet range
    services_secondary_range_name = "intranet-service-ipam-2"
  }

  resource_labels = {
    mesh_id="proj-${google_project.hub.number}"
  }

  workload_identity_config {
    identity_namespace = "${google_project.hub.project_id}.svc.id.goog"
  }

  authenticator_groups_config {
    security_group = "gke-security-groups@${var.security_group_domain}"
  }

  logging_service="logging.googleapis.com/kubernetes"
  monitoring_service="monitoring.googleapis.com/kubernetes"

  addons_config {

    dns_cache_config {
      enabled = true
    }
  }
}

resource "google_container_node_pool" "nodepool-2" {
  name       = "nodepool-2"
  location           = var.zone
  node_locations     = [var.zone]

  cluster    = google_container_cluster.cluster-2.name
  project    = google_project.hub.project_id
  initial_node_count = 2

  autoscaling {
    min_node_count = 2
    max_node_count = 4
  }

  node_config {
    machine_type = var.vm_type_runtime
    service_account = data.google_service_account.sa-cluster.email

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
        "https://www.googleapis.com/auth/cloud-platform"
    ]

    tags = [google_container_cluster.cluster-1.name, "cluster-runtime","cluster-${google_project.hub.project_id}"]
  }
}