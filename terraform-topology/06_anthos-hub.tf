module "hub" {
  source           = "terraform-google-modules/kubernetes-engine/google//modules/hub"

  project_id       = google_project.hub.project_id
  cluster_name     = google_container_cluster.cluster-1.name
  location         = google_container_cluster.cluster-1.location
  cluster_endpoint = google_container_cluster.cluster-1.endpoint
}
module "acm" {
  source           = "terraform-google-modules/kubernetes-engine/google//modules/acm"

  project_id       = google_project.hub.project_id
  cluster_name     = google_container_cluster.cluster-1.name
  location         = google_container_cluster.cluster-1.location
  cluster_endpoint = google_container_cluster.cluster-1.endpoint

  sync_repo        = "ssh://shawnho@google.com@source.developers.google.com:2022/p/shawn-demo-2021/r/config-repo"
  sync_branch      = "master"
  create_ssh_key   = false
  ssh_auth_key     = file("/Users/shawnho/.ssh/id_rsa")
  //depends_on       = [module.asm.wait]
}
module "gke_auth" {
  source           = "terraform-google-modules/kubernetes-engine/google//modules/auth"
  project_id       = google_project.hub.project_id
  cluster_name     = google_container_cluster.cluster-1.name
  location         = google_container_cluster.cluster-1.location
  use_private_endpoint = true
}
/*
module "asm" {
  source           = "terraform-google-modules/kubernetes-engine/google//modules/asm"
  version          = "17.1.0"
  asm_version      = "1.10"
  project_id       = google_project.hub.project_id
  cluster_name     = google_container_cluster.cluster-1.name
  location         = google_container_cluster.cluster-1.location
  cluster_endpoint = google_container_cluster.cluster-1.endpoint
  enable_all       = true
  managed_control_plane = true
  enable_registration   = false
  options          = ["cloud-tracing,internal-load-balancer"]
  outdir           = "asm-dir-${google_container_cluster.cluster-1.name}"
  skip_validation  = true
  //depends_on       = [module.hub.wait]
}
*/