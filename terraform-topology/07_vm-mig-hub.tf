resource "google_compute_instance_template" "intranet-vm-template" {
  name         = "intranet-vm-template"
  machine_type = "e2-small"
  disk {
    source_image = "debian-cloud/debian-10"
    auto_delete  = true
    boot         = true
  }
  network_interface {
    network    = google_compute_network.hub_vpc.id
    subnetwork = google_compute_subnetwork.hub_vpc_intranet.id
    access_config {
      // Ephemeral public IP
    }
  }
  project = google_project.hub.project_id
  metadata_startup_script = data.template_file.nginx-intranet.rendered
}
data "template_file" "nginx-intranet" {
  template = "${file("${path.module}/template/install_nginx.tpl")}"

  vars = {
    hostname = "Intranet-VM"
  }
}
# Intranet MIG
resource "google_compute_instance_group_manager" "intranet-mig" {
  name     = "intranet-mig"
  provider = google
  zone     = var.zone
  named_port {
    name = "http"
    port = 80
  }
  version {
    instance_template = google_compute_instance_template.intranet-vm-template.id
    name              = "primary"
  }
  base_instance_name = "intranet-vm"
  target_size        = 1
  project = google_project.hub.project_id
}
