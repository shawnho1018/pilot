# Terraform Automated Architecture
This project is to show how to use terraform to automate cross-project vpc peering. In order to demonstrate the firewall control, the script is designed to create the following resources.

* 2 Projects and 1 VPC per project.
* Each VPC has 1 subnet and 2 secondary network ranges.
* GCE instance in each VPC.
  * 1st project has a managed GCE instance group (with nginx installed as reverse proxy). The relay point of the reverse proxy is the Internal Http Loadbalancer in the 2nd project.
  * 2nd project has a managed GCE instance group and nginx installed as web server.
* External Http loadbalancer for the 1st project. Its backend is to the corresponding GCE in the same project.
* Internal Http loadbalancer for the 2nd project. Its backend is to the corresponding GCE in the same project.


Before executing, please modify shawn.tfvars according to your project name. 
Once it is done, we could retrieve the topology by running the following script:
```bash
terraform apply -auto-approve -var-file ./shawn.tfvars
```
# Test Service Mesh
This particular branch focused to test service-entry on managed-asm. We add a VM in the back of GKE cluster, as shown in ![Topology graph](image/service-entry.png)

To add istio, we utilize Anthos service mesh with managed control plane. My intention was to integrate terraform asm module but experienced a weird error. ASM module will complain the GKE cluster needs an identity provider without the detailed step. I therefore hacked the install_asm.sh script to complete the installation.

## Deploy Anthos Service Mesh 1.10
For readers who read this part, please download install_asm.sh and comment out this part.
```
#  if [[ -z "${IDENTITY_PROVIDER}" || "${IDENTITY_PROVIDER}" == 'null' ]]; then
#    { read -r -d '' MSG; fatal "${MSG}"; } <<EOF || true
#Cluster has memberships.hub.gke.io CRD but no identity provider specified. Please ensure that
#an identity provider is available for the registered cluster.
#EOF
#  fi
```

After modifying ./install_asm_1.10, run the following command to complete the installation. Please change project_id, cluster_name, cluster_location, according to your settings. 
```
./install_asm_1.10 --verbose --project_id shawn-hub-1 --cluster_name pcluster-1 --cluster_location asia-east1-a --mode install --managed --option cloud-tracing --option internal-load-balancer  --output_dir asm-dir-pcluster-1 --enable_all
```
## Deploy Istio-ingressgateway
After the managed istio 1.10 was installed, istio-ingressgateway requires to install separately with [the following steps](https://cloud.google.com/service-mesh/v1.10/docs/managed-service-mesh#install_istio_gateways_optional): 
```
kubectl label namespace istio-system istio.io/rev=asm-managed-rapid --overwrite

kubectl apply -f - << EOF
apiVersion: v1
kind: Service
metadata:
  name: istio-ingressgateway
  namespace: istio-system
spec:
  type: LoadBalancer
  selector:
    istio: ingressgateway
  ports:
  - port: 80
    name: http
  - port: 443
    name: https
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: istio-ingressgateway
  namespace: istio-system
spec:
  selector:
    matchLabels:
      istio: ingressgateway
  template:
    metadata:
      annotations:
        # This is required to tell Anthos Service Mesh to inject the gateway with the
        # required configuration.
        inject.istio.io/templates: gateway
      labels:
        istio: ingressgateway
        istio.io/rev: asm-managed-rapid # This is required only if the namespace is not labeled.
    spec:
      containers:
      - name: istio-proxy
        image: auto # The image will automatically update each time the pod starts.
---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: istio-ingressgateway-sds
  namespace: istio-system
rules:
- apiGroups: [""]
  resources: ["secrets"]
  verbs: ["get", "watch", "list"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: istio-ingressgateway-sds
  namespace: istio-system
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: istio-ingressgateway-sds
subjects:
- kind: ServiceAccount
  name: default
EOF
```

## Deploy K8s-manifests
Label default namespace with auto-injection label and apply yaml files in k8s-manifests folder.
```
kubectl label namespace default istio.io/rev=asm-managed-rapid --overwrite
kubectl apply -f k8s-manifests/
```

Our serviceentry and the corresponding virtualservice is shown below, where 10.3.0.5 is the ip address of the GCE VM, deployed with 07_vm-mig-hub.tf. 
```
terraform-topology % cat k8s-manifests/service-entry.yaml 
apiVersion: networking.istio.io/v1beta1
kind: ServiceEntry
metadata:
  name: vm-nginx
  namespace: istio-system
spec:
  hosts:
  - intranet.vs
  location: MESH_EXTERNAL
  ports:
  - number: 80
    name: http
    protocol: TCP
  resolution: STATIC
  endpoints:
  - address: 10.3.0.5
---
apiVersion: networking.istio.io/v1beta1
kind: VirtualService
metadata:
  name: intranet-vm 
  namespace: istio-system
spec:
  hosts:
  - intranet.shawn.test
  gateways:
  - nginx-ingress  
  http:
  - route:
    - destination:
        host: intranet.vs
        port:
          number: 80
      weight: 100%   
```

Once the deploymenet is completed, we have two virtual services linked to the same istio gateway. One is deployed by service-entry.yaml with a hostname of intranet.shawn.test. The other is deployed by nginx-vs.yaml with a hostname of nginx-1.shawn.test. We could test from our client with the istio-ingressgateway loadbalancer IP: 
```
terraform-topology % kubectl get svc -n istio-system 
NAME                   TYPE           CLUSTER-IP     EXTERNAL-IP    PORT(S)                      AGE
istio-ingressgateway   LoadBalancer   10.101.0.160   34.80.23.164   80:30768/TCP,443:31945/TCP   17h

terraform-topology % curl -H "Host: intranet.shawn.test" 34.80.23.164
<html>
<body>
<p> Welcome to Google Compute VM 'Intranet-VM' </p>
</body>
</html>

terraform-topology % curl -H "Host: nginx-1.shawn.test" 34.80.23.164
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

```