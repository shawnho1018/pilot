#!/bin/bash
set -e
echo "*****    Installing Nginx    *****"
apt update
apt install -y nginx
systemctl enable nginx
systemctl restart nginx
echo "****     Create index.html   ****"
cat <<EOF > /var/www/html/index.html
<html>
<body>
<p> Welcome to Google Compute VM '${hostname}' </p>
</body>
</html>
EOF
ls -l /var/www/html
