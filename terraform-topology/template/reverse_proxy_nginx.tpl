#!/bin/bash
set -e
echo "*****    Installing Nginx    *****"
apt update
apt install -y nginx
systemctl enable nginx

cat<<EOF > /etc/nginx/sites-available/default
server {
	listen 80 default_server;
	listen [::]:80 default_server;
        proxy_http_version   1.1;
	location / {
		proxy_pass http://${backend};
	}
}
EOF

systemctl restart nginx
