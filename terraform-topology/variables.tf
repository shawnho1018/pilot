variable "project_id" {
  type = string 
  description = "Project to store terraform state variables"
}
variable "hub_project_id" {
  type = string
}
variable "spoke_project_id" {
  type = string
}
variable "region" {
  type = string
}
variable "zone" {
  type = string
}

variable "google_private_ips" {
  description = "Private Google API IPs see: https://cloud.google.com/vpc/docs/configure-private-google-access"
  default = ["199.36.153.8", "199.36.153.9", "199.36.153.10", "199.36.153.11"]
}

variable "cluster_service_account_id" {
  description = "" # TODO
}
variable "security_group_domain" {
  description = "name of the security group"
}
variable "vm_type_runtime" {
  default = "n1-standard-4"
}

variable "vm_type_data" {
  default = "n1-standard-4"
}
