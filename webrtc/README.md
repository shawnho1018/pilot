# Dynamic Route with Istio
## Lab1: Quick Proof-of-Concept (POC)
This workshop is to demonstrate how to adapt istio for dynamic traffic redirection. In order to simulate the "dynamic" nature, there should be a pod which dynamically control kubernetes, as shown in [kubernetes client-go](https://github.com/kubernetes/client-go/tree/master/examples/in-cluster-client-configuration). In the POC, we'll first utilize bash script to "represent" the simulate the dynamic behavior. 

In this lab, there are 2 steps:
1. 0-init.sh: This script simulates a group of pods are created from a k8s deployment for further utilizing (e.g. transcode or decode). If audience desires to have autoscaler, they could easily add an HPA (horizontal pod autoscaler) to the simulation. Each pod has a common label for its k8s-service to access. We also config our general service hostname as ("whereami.shawnk8s.com") and default subpath ("/") are able to access this service in our istio-ingressgateway. Audience could run the following script multiple times and expect the requests were evenly distributed to 3 different pods. 
```
curl -H "HOST: whereami.shawnk8s.com" http://[istio-ingressgateway ip address]
```

2. 1-host-register.sh: This script simulates if a host requests to have his/her service published. We could quickly label a pod, create an exclusive k8s-service, and register a subpath (e.g. "/host1") for this host. Once the script is executed, audience could test curl to the following website. All requests will only be directed to one of the 3 pods to have the traffic affinity.
```
curl -H "HOST: whereami.shawnk8s.com" http://[istio-ingressgateway ip address]/host1
```

The lab topology is shown below:
![webrtc-topology1](images/webrtc-topology1.png)


